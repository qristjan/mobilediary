package ut.student.puusepp.kristjan.mobilediary;

import android.util.Log;

import java.util.Comparator;

class Reminder {

    private int _id;
    private String _titleOfReminder;
    private String _contentOfReminder;
    private String _dateOfCreation;
    private String _reminderCompleted;
    private String _dateOfExpiration;
    private String _timeOfExpiration;
    private String _userid;
    private String _sid;

    Reminder() {

    }

    Reminder(int _id, String _titleOfReminder, String _contentOfReminder, String _dateOfCreation, String _dateOfExpiration, String _timeOfExpiration, String _reminderCompleted, String _userid, String _sid) {
        this._id = _id;
        this._titleOfReminder = _titleOfReminder;
        this._contentOfReminder = _contentOfReminder;
        this._dateOfCreation = _dateOfCreation;
        this._reminderCompleted = _reminderCompleted;
        this._dateOfExpiration = _dateOfExpiration;
        this._timeOfExpiration = _timeOfExpiration;
        this._userid = _userid;
        this._sid = _sid;
    }

    Reminder(String _titleOfReminder, String _contentOfReminder, String _dateOfCreation, String _dateOfExpiration, String _timeOfExpiration, String _reminderCompleted, String _userid, String _sid) {
        this._titleOfReminder = _titleOfReminder;
        this._contentOfReminder = _contentOfReminder;
        this._dateOfCreation = _dateOfCreation;
        this._reminderCompleted = _reminderCompleted;
        this._dateOfExpiration = _dateOfExpiration;
        this._timeOfExpiration = _timeOfExpiration;
        this._userid = _userid;
        this._sid = _sid;
    }

    public String get_timeOfExpiration() {
        return _timeOfExpiration;
    }

    public void set_timeOfExpiration(String _timeOfExpiration) {
        this._timeOfExpiration = _timeOfExpiration;
    }

    Reminder(String _titleOfReminder, String _contentOfReminder, String _dateOfCreation, String _dateOfExpiration, String _timeOfExpiration, String _userid, String _sid) {
        this._titleOfReminder = _titleOfReminder;
        this._contentOfReminder = _contentOfReminder;
        this._dateOfCreation = _dateOfCreation;
        this._dateOfExpiration = _dateOfExpiration;
        this._timeOfExpiration = _timeOfExpiration;
        this._userid = _userid;
        this._sid = _sid;

    }


    public String get_userid() {
        return _userid;
    }

    public void set_userid(String _userid) {
        this._userid = _userid;
    }


    public String get_dateOfExpiration() {
        return _dateOfExpiration;
    }

    public void set_dateOfExpiration(String _dateOfExpiration) {
        this._dateOfExpiration = _dateOfExpiration;
    }

    int get_id() {
        return _id;
    }

    void set_id(int _id) {
        this._id = _id;
    }

    public String is_reminderCompleted() {
        return _reminderCompleted;
    }

    public void set_reminderCompleted(String _reminderCompleted) {
        this._reminderCompleted = _reminderCompleted;
    }

    String get_titleOfReminder() {
        return _titleOfReminder;
    }

    void set_titleOfReminder(String _titleOfReminder) {
        this._titleOfReminder = _titleOfReminder;
    }

    String get_contentOfReminder() {
        return _contentOfReminder;
    }

    void set_contentOfReminder(String _contentOfReminder) {
        this._contentOfReminder = _contentOfReminder;
    }

    public String get_sid() {
        return _sid;
    }
    public void set_sid(String _sid) {
        this._sid = _sid;
    }

    String get_dateOfCreation() {
        return _dateOfCreation;
    }

    void set_dateOfCreation(String _dateOfCreation) {
        this._dateOfCreation = _dateOfCreation;
    }

    //"dd.MM.yyyy HH:mm" - stored in DB this way

    static public Long formatToComparable(String date) {
        Log.w(Variables.TAG, date);
        String[] pieces = date.split(" ");
        String[] datepieces = pieces[0].replace(" ", "").split("\\.");
        String[] timepieces = pieces[1].replace(" ", "").split(":");
        String temp = datepieces[2] + datepieces[1] + datepieces[0] + timepieces[0] + timepieces[1];
        return Long.parseLong(temp);
    }


    static Comparator<Reminder> orderByCreationDate() {
        return new Comparator<Reminder>() {
            @Override
            public int compare(Reminder o1, Reminder o2) {
                if (o1.get_dateOfCreation() == null || o2.get_dateOfCreation() == null)
                    return 0;
                Long first = Long.parseLong(o1.get_dateOfCreation());
                Long second = Long.parseLong(o2.get_dateOfCreation());
                return first.compareTo(second);
            }
        };
    }

    static Comparator<Reminder> orderByEndDate() {
        return new Comparator<Reminder>() {
            @Override
            public int compare(Reminder o1, Reminder o2) {
                if (o1 == null || o2 == null) {
                    return 0;
                }
                String frst = "";
                if (!o1.get_dateOfExpiration().isEmpty() && !o1.get_timeOfExpiration().isEmpty()) {
                    frst = o1.get_dateOfExpiration() + " " + o1.get_timeOfExpiration();
                }
                if (o1.get_dateOfExpiration().isEmpty()) {
                    frst = "10.10.1000";
                }
                if (o1.get_timeOfExpiration().isEmpty()) {
                    frst = frst + " 00:00";
                }

                String scnd = "";
                if (!o2.get_dateOfExpiration().isEmpty() && !o2.get_timeOfExpiration().isEmpty()) {
                    scnd = o2.get_dateOfExpiration() + " " + o2.get_timeOfExpiration();
                }
                if (o2.get_dateOfExpiration().isEmpty()) {
                    scnd = "10.10.1000";
                }
                if (o2.get_timeOfExpiration().isEmpty()) {
                    scnd = scnd + " 00:00";
                }
                try {
                    Long first = formatToComparable(frst);
                    Long second = formatToComparable(scnd);
                    return first.compareTo(second);
                } catch (Exception e) {
                    return -1;
                }
            }
        };
    }

    static Comparator<Reminder> orderByTitle() {
        return new Comparator<Reminder>() {
            @Override
            public int compare(Reminder s1, Reminder s2) {
                return s1.get_titleOfReminder().compareToIgnoreCase(s2.get_titleOfReminder());
            }
        };
    }
}


