package ut.student.puusepp.kristjan.mobilediary;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class RegisterActivity extends AppCompatActivity {

    EditText email;
    EditText password;
    EditText password2;
    final DatabaseHandler db = new DatabaseHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        email = findViewById(R.id.regusername);
        password = findViewById(R.id.regpassword);
        password2 = findViewById(R.id.regpassword2);

        final Button registerBtn = findViewById(R.id.register);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (email.getText().toString().equals("") || password.getText().toString().equals("") || password2.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), getResources().getText(R.string.fieldscannotbeempty), Toast.LENGTH_SHORT).show();
                } else if (!isEmailValid(email.getText().toString())) {
                    Toast.makeText(getApplicationContext(), getResources().getText(R.string.notvalidemail), Toast.LENGTH_SHORT).show();
                } else if (!password.getText().toString().equals(password2.getText().toString())) {
                    Toast.makeText(getApplicationContext(), getResources().getText(R.string.passaredifferent), Toast.LENGTH_SHORT).show();
                } else {
                    new RegisterAsyncTask().execute();
                }
            }
        });
    }

    private boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private class RegisterAsyncTask extends AsyncTask<Void, Void, String> {

        protected String doInBackground(Void... voids) {
            try {
                URL url = new URL(Variables.REGISTER);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json");
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setConnectTimeout(5000);

                JSONObject jsonParam = new JSONObject();
                jsonParam.put("head", "createuser");
                JSONArray jArray = new JSONArray();
                JSONObject registerInfo = new JSONObject();
                registerInfo.put("email", email.getText().toString());
                registerInfo.put("password", password.getText().toString());
                jArray.put(registerInfo);
                jsonParam.put("userinfo", jArray);
                DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                os.writeBytes(jsonParam.toString());

                os.flush();
                os.close();

                final String result = IOUtils.toString(conn.getInputStream(), StandardCharsets.UTF_8);


                Log.i("STATUS", String.valueOf(conn.getResponseCode()));
                Log.i("MSG", conn.getResponseMessage());
                Log.i("RESPONSE", result);
                conn.disconnect();
                return result;
            } catch (Exception e) {
                e.printStackTrace();
                return "errot";
            }

        }

        @Override
        protected void onPostExecute(String result) {
            switch (result) {
                case "error":
                    Toast.makeText(getApplicationContext(), getResources().getText(R.string.wrong), Toast.LENGTH_SHORT).show();
                    break;
                case "existss":
                    Toast.makeText(RegisterActivity.this, getResources().getText(R.string.emailinuse), Toast.LENGTH_SHORT).show();
                    break;
                default:
                    db.updateUserActivityToFalse();
                    db.addUser(new User(result, email.getText().toString(), "t"));
                    Toast.makeText(getApplicationContext(), getResources().getText(R.string.usercreated), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                    break;
            }
        }

    }

}
