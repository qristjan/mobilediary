package ut.student.puusepp.kristjan.mobilediary;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;

public class AppPreferences {

    private static final String ASK_FOR_EXPENSES_DATA = "askforexpenses";
    private static final String EXPENSES_DATA_ALLOWED = "expensesallowed";
    private static final String LAST_MONTH_UPDATED = "lastmonthupdated";

    public static Integer monthWhenDataLastSent(Context context) {
        final SharedPreferences reader = context.getSharedPreferences(LAST_MONTH_UPDATED, Context.MODE_PRIVATE);
        final Integer month = reader.getInt("lastUpdatesMonth", 0);
        return month;
    }

    public static void setMonthWhenDataSent(Context context, Integer month) {
        final SharedPreferences reader = context.getSharedPreferences(LAST_MONTH_UPDATED, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = reader.edit();
        editor.putInt("lastUpdatesMonth", month);
        editor.apply();
    }

    public static boolean isAnswered(Context context){
        final SharedPreferences reader = context.getSharedPreferences(ASK_FOR_EXPENSES_DATA, Context.MODE_PRIVATE);
        final boolean isAnswered = reader.getBoolean("asked", false);
        return isAnswered;
    }

    public static void setAnswered(Context context, boolean isAnswered) {
        final SharedPreferences reader = context.getSharedPreferences(ASK_FOR_EXPENSES_DATA, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = reader.edit();
        editor.putBoolean("asked", isAnswered);
        editor.apply();
    }

    public static boolean isExpensesDataAllowed(Context context){
        final SharedPreferences reader = context.getSharedPreferences(EXPENSES_DATA_ALLOWED, Context.MODE_PRIVATE);
        final boolean first = reader.getBoolean("allowed", false);
        return first;
    }

    public static void setExpensesDataAllowed(Context context, boolean isAllowed) {
        final SharedPreferences reader = context.getSharedPreferences(EXPENSES_DATA_ALLOWED, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = reader.edit();
        editor.putBoolean("allowed", isAllowed);
        editor.apply();
    }

}
