package ut.student.puusepp.kristjan.mobilediary;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class GroupMembersActivity extends AppCompatActivity {

    ArrayList<String> groupMembers = new ArrayList<>();
    ListView listView;
    GroupMembersListAdapter groupMembersListAdapter;
    DatabaseHandler databaseHandler = new DatabaseHandler(this);
    String groupId;
    Context context;
    SwipeRefreshLayout mySwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_members);
        context = this;

        Intent intent = getIntent();
        groupId = intent.getStringExtra("groupId");

        listView = findViewById(R.id.listOfMembers);
        groupMembersListAdapter = new GroupMembersListAdapter(this, R.layout.groupmember_item, groupMembers);
        listView.setAdapter(groupMembersListAdapter);

        new GetGroupMembersAsyncTask(0, this, databaseHandler.getActiveUser().getUserid(), groupId, Variables.GET_GROUP_MEMBERS).execute();

        mySwipeRefreshLayout = findViewById(R.id.swiperefreshgroupmembers);
        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        GetGroupMembersAsyncTask getGroupMembersAsyncTask = new GetGroupMembersAsyncTask(0, GroupMembersActivity.this, databaseHandler.getActiveUser().getUserid(), groupId, Variables.GET_GROUP_MEMBERS);
                        getGroupMembersAsyncTask.execute();
                    }
                }
        );

        final FloatingActionButton addNewMember = findViewById(R.id.addMember);
        addNewMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialog_add_member);

                final EditText newGroupMemberEmail = dialog.findViewById(R.id.newMemberEmail);
                final Button saveNewMemberButton = dialog.findViewById(R.id.saveNewMember);

                saveNewMemberButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        GetGroupMembersAsyncTask getGroupMembersAsyncTask = new GetGroupMembersAsyncTask(1, GroupMembersActivity.this, databaseHandler.getActiveUser().getUserid(), groupId, Variables.GROUP_SETTINGS, newGroupMemberEmail.getText().toString(), "addmember", -1);
                        getGroupMembersAsyncTask.execute();
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(GroupMembersActivity.this, GroupsActivity.class);
        startActivity(intent);
    }

    private class GroupMembersListAdapter extends ArrayAdapter<String> {
        private int layout;
        private List<String> groupMembers;

        public GroupMembersListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<String> _groupMembers) {
            super(context, resource, _groupMembers);
            layout = resource;
            groupMembers = _groupMembers;
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull final ViewGroup parent) {
            ViewHolder mainViewHolder;
            if (convertView == null) {

                final LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(layout, parent, false);

                final ViewHolder viewHolder = new ViewHolder();

                viewHolder.memberEmail = convertView.findViewById(R.id.list_groupmember_item);
                viewHolder.memberEmail.setText(groupMembers.get(position));

                viewHolder.kickMember = convertView.findViewById(R.id.groupmember_kick);
                viewHolder.kickMember.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        GetGroupMembersAsyncTask getGroupMembersAsyncTask = new GetGroupMembersAsyncTask(1, GroupMembersActivity.this, databaseHandler.getActiveUser().getUserid(), groupId, Variables.GROUP_SETTINGS, groupMembers.get(position), "kickmember", position);
                        getGroupMembersAsyncTask.execute();

                    }
                });

                viewHolder.makeMemberAdmin = convertView.findViewById(R.id.groupmember_makeadmin);
                viewHolder.makeMemberAdmin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        GetGroupMembersAsyncTask getGroupMembersAsyncTask = new GetGroupMembersAsyncTask(1, GroupMembersActivity.this, databaseHandler.getActiveUser().getUserid(), groupId, Variables.GROUP_SETTINGS, groupMembers.get(position), "makeadmin", position);
                        getGroupMembersAsyncTask.execute();
                    }
                });

                if (databaseHandler.getActiveUser().getEmail().equals(groupMembers.get(position))) {
                    viewHolder.kickMember.setVisibility(View.GONE);
                    viewHolder.makeMemberAdmin.setVisibility(View.GONE);
                }

                convertView.setTag(viewHolder);

            }
            else {

                mainViewHolder = (GroupMembersActivity.ViewHolder) convertView.getTag();
                mainViewHolder.memberEmail.setText(getItem(position));

            }
            //return super.getView(position, convertView, parent);
            return convertView;
        }
    }

    public class ViewHolder {
        TextView memberEmail;
        Button makeMemberAdmin;
        Button kickMember;
    }

    private static class GetGroupMembersAsyncTask extends AsyncTask<Void, Void, String> {

        private WeakReference<GroupMembersActivity> activityReference;
        private String userId;
        private String groupId;
        private String API_URL;
        private String memberEmail;
        private Integer taskNumber;
        private String task;
        private int position;

        GetGroupMembersAsyncTask(Integer taskNumber, GroupMembersActivity context, String userId, String groupId, String API_URL) {
            activityReference = new WeakReference<>(context);
            this.userId = userId;
            this.taskNumber = taskNumber;
            this.groupId = groupId;
            this.API_URL = API_URL;
        }

        GetGroupMembersAsyncTask(Integer taskNumber, GroupMembersActivity context, String userId, String groupId, String API_URL, String memberEmail,  String task, int position) {
            activityReference = new WeakReference<>(context);
            this.userId = userId;
            this.groupId = groupId;
            this.API_URL = API_URL;
            this.memberEmail = memberEmail;
            this.task = task;
            this.taskNumber = taskNumber;
        }


        protected String doInBackground(Void... voids) {
            try {
                GroupMembersActivity activity = activityReference.get();
                URL url = new URL(API_URL);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json");
                conn.setDoOutput(true);
                conn.setDoInput(true);

                JSONObject jsonParam = new JSONObject();
                if (taskNumber == 0) {
                    jsonParam.put("head", "getgroupmembers");
                    jsonParam.put("userid", userId);
                    jsonParam.put("groupid", groupId);
                } else if (taskNumber == 1) {
                    jsonParam = createJSON(task, userId, groupId, memberEmail);
                }

                Log.i("JSON", jsonParam.toString());

                DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                os.writeBytes(jsonParam.toString());

                os.flush();
                os.close();

                final String result = IOUtils.toString(conn.getInputStream(), StandardCharsets.UTF_8);

                if (taskNumber == 0) {
                    activity.groupMembers.clear();
                    JSONObject jsonresult = new JSONObject(result);
                    JSONArray jsonArray = jsonresult.getJSONArray("members");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        activity.groupMembers.add(jsonArray.getString(i));
                    }
                }


                Log.i("STATUS", String.valueOf(conn.getResponseCode()));
                Log.i("MSG", conn.getResponseMessage());
                Log.i("RESPONSE", result);
                conn.disconnect();

                if (taskNumber == 1) {
                    return result;
                }
                return "done";

            } catch (Exception e) {
                e.printStackTrace();
                Log.i(Variables.TAG, "WHAT==");
                return "exception";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            GroupMembersActivity activity = activityReference.get();
            Log.i(Variables.TAG, "Result: " + result);
            switch (result) {
                case "added":
                    Toast.makeText(activity, activity.getResources().getText(R.string.addednewmember), Toast.LENGTH_LONG).show();
                    if (taskNumber == 1) {
                        activity.groupMembers.add(memberEmail);
                    }
                    break;
                case "deleted":
                    Toast.makeText(activity, activity.getResources().getText(R.string.deletegroup), Toast.LENGTH_LONG).show();
                    break;
                case "left":
                    Toast.makeText(activity, activity.getResources().getText(R.string.leftgroup), Toast.LENGTH_LONG).show();
                    break;
                case "changed":
                    Toast.makeText(activity, activity.getResources().getText(R.string.namechanged), Toast.LENGTH_LONG).show();
                    break;
                case "kicked":
                    activity.groupMembers.remove(position);
                    Toast.makeText(activity, activity.getResources().getText(R.string.kicked), Toast.LENGTH_LONG).show();
                    break;
                case "admin":
                    Toast.makeText(activity, activity.getResources().getText(R.string.madeadmin), Toast.LENGTH_LONG).show();
                case "error":
                    Log.i(Variables.TAG, "Result: ERROR");
                    Toast.makeText(activity, "Connection lost", Toast.LENGTH_LONG).show();
                    break;
                case "notadmin":
                    Toast.makeText(activity, activity.getResources().getText(R.string.notadmin), Toast.LENGTH_LONG).show();
                    break;
                case "none":
                    Toast.makeText(activity, activity.getResources().getText(R.string.emailnotinuse), Toast.LENGTH_LONG).show();
                    break;
                case "done":
                    break;
                case "exception":
                    Log.i(Variables.TAG, "Result: EXCEPTION");
                    activity.mySwipeRefreshLayout.setRefreshing(false);
                    Toast.makeText(activity, "Connection lost", Toast.LENGTH_LONG).show();
                    break;
            }
            activity.mySwipeRefreshLayout.setRefreshing(false);
            activity.groupMembersListAdapter.notifyDataSetChanged();
        }
    }

    private static JSONObject createJSON(String task, String userid, String groupId, String memberEmail) throws Exception {
        JSONObject jsonParam = new JSONObject();
        jsonParam.put("head", "groupsettings");
        jsonParam.put("userid", userid);
        jsonParam.put("task", task);
        jsonParam.put("groupid", groupId);
        if (task.equals("kickmember") || task.equals("addmember") || task.equals("makeadmin")) {
            jsonParam.put("email", memberEmail);
        }
        return jsonParam;
    }
}
