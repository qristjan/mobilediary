package ut.student.puusepp.kristjan.mobilediary;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    List<Reminder> remindersNoContent;
    ListView listView;
    ReminderListAdapter reminderListAdapter;

    DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        databaseHandler = new DatabaseHandler(this);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavView_Bar);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.reminders:

                        break;
                    case R.id.groups:
                        if (databaseHandler.getActiveUser().getUserid().equals("local")) {
                            Toast.makeText(MainActivity.this, getResources().getText(R.string.needtobelogged), Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent1 = new Intent(MainActivity.this, GroupsActivity.class);
                            startActivity(intent1);
                            finish();
                        }
                        break;

                    case R.id.expenses:
                        Intent intent2 = new Intent(MainActivity.this, ExpensesActivity.class);
                        startActivity(intent2);
                        finish();
                        break;
                }
                return false;
            }
        });

        /*
        Getting all the reminders without content (to reduce the load)
         */
        if (databaseHandler.getShowCompleted() == 0) {
            remindersNoContent = databaseHandler.getAllReminderTitles();
        } else {
            remindersNoContent = databaseHandler.getAllIncompletedReminders();
        }

        /*
        Creating the Reminders View list of reminders
         */
        listView = findViewById(R.id.listOfReminders);
        reminderListAdapter = new ReminderListAdapter(this, R.layout.reminder_item, remindersNoContent);
        listView.setAdapter(reminderListAdapter);

        /*
        Button for creating new reminders/ changing existing reminders
        Starts new activity CreateReminderActivity...
        ...with empty template to create new reminder
        ...or with existing data of a reminder (so the user can modify it)
         */
        FloatingActionButton createNewReminder = findViewById(R.id.fab);
        createNewReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CreateReminderActivity.class);
                intent.putExtra("ce", "pcreate");
                startActivity(intent);
                finish();
            }
        });

        /*
        Creating spinner for ordering the reminders by: creation date, end date, title
         */
        Spinner spinner = findViewById(R.id.reminder_ordering_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.reminders_ordering_items, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings_mainactivity, menu);
        if (databaseHandler.getActiveUser().getEmail().equals("local")) {
            menu.getItem(3).setTitle("Login");
        }
        if (databaseHandler.getShowCompleted() == 1) {
            menu.getItem(2).setTitle("Show completed");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.settings:
                Intent intent2 = new Intent(MainActivity.this, SettingsActivity.class);
                intent2.putExtra("class", "mainactivity");
                startActivity(new Intent(intent2));
                finish();
                return true;
            case R.id.sync:
                if (databaseHandler.getActiveUser().getUserid().equals("local")) {
                    Toast.makeText(this, getResources().getText(R.string.needtobelogged), Toast.LENGTH_SHORT).show();
                } else {
                    RemindersAsyncTask remindersAsyncTask = new RemindersAsyncTask(this, databaseHandler.getExistingSIDs(), databaseHandler.getAllLocalReminders(), databaseHandler.getActiveUser().getUserid(), Variables.SYNC_PERSONAL_REMINDERS);
                    remindersAsyncTask.execute();
                }
                return true;
            case R.id.hidecompleted:
                if (databaseHandler.getShowCompleted() == 1) {
                    databaseHandler.changeShowCompletedToTrue();
                } else {
                    databaseHandler.changeShowCompletedToFalse();
                }
                recreate();
                return true;
            case R.id.logout:
                if (databaseHandler.getActiveUser().getEmail().equals("local")) {
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    finish();
                } else {
                    databaseHandler.updateUserActivityToFalse();
                    Intent intent = new Intent(MainActivity.this, FirstScreenActivity.class);
                    startActivity(intent);
                    finish();
                }
                return true;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    private class ReminderListAdapter extends ArrayAdapter<Reminder> {
        private int layout;
        private List<Reminder> remindersNoContent;

        public ReminderListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Reminder> _reminderNoContent) {
            super(context, resource, _reminderNoContent);
            layout = resource;
            remindersNoContent = _reminderNoContent;
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull final ViewGroup parent) {
            ViewHolder mainViewHolder;
            if (convertView == null) {
                String title = remindersNoContent.get(position).get_titleOfReminder();
                String endDateTime = remindersNoContent.get(position).get_dateOfExpiration() + "  " + remindersNoContent.get(position).get_timeOfExpiration();

                final LayoutInflater inflater = LayoutInflater.from(getContext());
                final ViewHolder viewHolder = new ViewHolder();
                convertView = inflater.inflate(layout, parent, false);

                viewHolder.title = convertView.findViewById(R.id.list_item_text);
                if (title.isEmpty()) viewHolder.title.setText(getResources().getText(R.string.notitle));
                else viewHolder.title.setText(title);

                viewHolder.enddate = convertView.findViewById(R.id.list_item_textenddate);
                viewHolder.enddate.setText(endDateTime);

                viewHolder.button = convertView.findViewById(R.id.list_item_button);
                viewHolder.button.setBackground(getDrawable(R.drawable.ic_check_black_24dp));
                if (remindersNoContent.get(position).is_reminderCompleted().equals("F")) viewHolder.button.setBackground(getDrawable(R.drawable.ic_check_box_outline_blank_black_48dp));


                viewHolder.button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String status = databaseHandler.getReminderStatus(remindersNoContent.get(position).get_id());
                        Log.w(Variables.TAG, status);
                        switch (status) {
                            case "T":
                                viewHolder.button.setBackground(getDrawable(R.drawable.ic_check_box_outline_blank_black_48dp));
                                databaseHandler.updateReminderStatus(remindersNoContent.get(position));
                                break;
                            case "F":
                                viewHolder.button.setBackground(getDrawable(R.drawable.ic_check_black_24dp));
                                databaseHandler.updateReminderStatus(remindersNoContent.get(position));
                                break;
                        }
                        //recreate();
                    }
                });

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainActivity.this, CreateReminderActivity.class);
                        intent.putExtra("ce", "ptrue");
                        intent.putExtra("id", remindersNoContent.get(position).get_id());
                        startActivity(intent);
                        finish();
                    }
                });
                convertView.setTag(viewHolder);

            }
            else {
                mainViewHolder = (ViewHolder) convertView.getTag();
                mainViewHolder.title.setText(getItem(position).get_titleOfReminder());
            }
            return convertView;
        }
    }

    public class ViewHolder {
        TextView title;
        TextView enddate;
        ImageButton button;
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        switch (pos) {
            case 0:
                break;
            case 1:
                Collections.sort(remindersNoContent, Reminder.orderByCreationDate());
                break;
            case 2:
                Collections.sort(remindersNoContent, Reminder.orderByCreationDate());
                Collections.reverse(remindersNoContent);
                break;
            case 3:
                Collections.sort(remindersNoContent, Reminder.orderByEndDate());
                break;
            case 4:
                Collections.sort(remindersNoContent, Reminder.orderByEndDate());
                Collections.reverse(remindersNoContent);
                break;
            case 5:
                Collections.sort(remindersNoContent, Reminder.orderByTitle());
                break;
            case 6:
                Collections.sort(remindersNoContent, Reminder.orderByTitle());
                Collections.reverse(remindersNoContent);
                break;
        }
        listView.setAdapter(reminderListAdapter);
        reminderListAdapter.notifyDataSetChanged();
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }


    private static class RemindersAsyncTask extends AsyncTask<Void, Void, Integer> {

        private WeakReference<MainActivity> activityReference;
        private String userId;
        private String API_URL;
        private DatabaseHandler databaseHandler;
        private List<Integer> existingServerIds;
        private List<Reminder> reminders;

        RemindersAsyncTask(MainActivity context, List<Integer> existingServerIds , List<Reminder> reminders, String userId, String API_URL) {
            activityReference = new WeakReference<>(context);
            this.existingServerIds = existingServerIds;
            this.userId = userId;
            this.API_URL = API_URL;
            this.reminders = reminders;
            databaseHandler = new DatabaseHandler(context);

        }

        protected Integer doInBackground(Void... voids) {
            try {
                URL url = new URL(API_URL);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json");
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setConnectTimeout(5000);

                ArrayList<Integer> tempIds = new ArrayList<>();

                JSONObject jsonParam = new JSONObject();
                jsonParam.put("userid", userId);


                JSONArray jArray1 = new JSONArray();
                for (String i : databaseHandler.getDeletedPersonalRemindersSIDs()) {
                    jArray1.put(i);
                }
                jsonParam.put("deletedrems", jArray1);

                jsonParam.put("head", "syncpersonal");
                JSONArray jArray = new JSONArray();
                for (Integer i : existingServerIds) {
                    jArray.put(i);
                }
                jsonParam.put("existingreminders", jArray);
                jsonParam.put("reminderssize", String.valueOf(reminders.size()));
                for (int i = 0; i < reminders.size(); i++) {

                    JSONArray jArray3 = new JSONArray();
                    jArray3.put(reminders.get(i).get_titleOfReminder());
                    jArray3.put(reminders.get(i).get_contentOfReminder());
                    jArray3.put(reminders.get(i).get_dateOfCreation());
                    jArray3.put(reminders.get(i).get_dateOfExpiration());
                    jArray3.put(reminders.get(i).get_timeOfExpiration());
                    jArray3.put(reminders.get(i).is_reminderCompleted());
                    jArray3.put(reminders.get(i).get_id());
                    tempIds.add(reminders.get(i).get_id());
                    jsonParam.put(String.valueOf(i), jArray3);

                }


                Log.i("JSON", jsonParam.toString());

                DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                os.writeBytes(jsonParam.toString());

                os.flush();
                os.close();

                final String result = IOUtils.toString(conn.getInputStream(), StandardCharsets.UTF_8);

                JSONObject jsonresult = new JSONObject(result);
                for (int i = 1; i < Integer.parseInt(jsonresult.getString("len")) + 1; i++) {
                    JSONArray jsonresultarray = jsonresult.getJSONArray(String.valueOf(i));
                    databaseHandler.addReminderSync(new Reminder(jsonresultarray.get(2).toString(), jsonresultarray.get(3).toString(), jsonresultarray.get(4).toString(), jsonresultarray.get(5).toString(), jsonresultarray.get(6).toString(), jsonresultarray.get(7).toString(), userId, jsonresultarray.get(0).toString()));
                }

                JSONObject jsonObject = jsonresult.getJSONObject("remids");
                for (Integer i : tempIds) {
                    databaseHandler.updateReminderSID(String.valueOf(i), jsonObject.getString(String.valueOf(i)));
                }

                Log.i("STATUS", String.valueOf(conn.getResponseCode()));
                Log.i("MSG", conn.getResponseMessage());
                Log.i("RESPONSE", result);
                conn.disconnect();
                return 0;
            } catch (Exception e) {
                e.printStackTrace();
                return 1;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            MainActivity mainActivity = activityReference.get();
            if (result == 0) {
                mainActivity.recreate();
                Toast.makeText(mainActivity, mainActivity.getResources().getText(R.string.synced), Toast.LENGTH_SHORT).show();
            }
            else if (result == 1) {
                Toast.makeText(mainActivity, "Connection lost", Toast.LENGTH_LONG).show();
            }
        }
    }


}
