package ut.student.puusepp.kristjan.mobilediary;

import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ExpensesActivity extends AppCompatActivity {

    PieChart pieChart;
    ArrayList<Float> yData = new ArrayList<>();
    ArrayList<String> xData = new ArrayList<>();
    List<Expense> expensesAll;
    Map<String, Float> categoryExpenses;
    TextView sumOfExpenses;
    String sum;
    TextView sumsOfCategories;
    String sumC;

    DatabaseHandler databaseHandler = new DatabaseHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expenses);

        if(!AppPreferences.isAnswered(ExpensesActivity.this)) {
            final Dialog dialog = new Dialog(ExpensesActivity.this);
            dialog.setContentView(R.layout.dialog_ask_for_expenses_data);

            final Button allow = dialog.findViewById(R.id.allow);
            final Button dontallow = dialog.findViewById(R.id.dontallow);

            allow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    AppPreferences.setExpensesDataAllowed(ExpensesActivity.this, true);
                    AppPreferences.setAnswered(ExpensesActivity.this, true);
                    AppPreferences.setMonthWhenDataSent(ExpensesActivity.this, -1);
                }
            });

            dontallow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    AppPreferences.setExpensesDataAllowed(ExpensesActivity.this, false);
                    AppPreferences.setAnswered(ExpensesActivity.this, true);
                }
            });

            dialog.show();
        }

        if(isNetworkAvailable() && AppPreferences.isExpensesDataAllowed(this) && (AppPreferences.monthWhenDataLastSent(this) != Calendar.getInstance().get(Calendar.MONTH))) {
            if(Calendar.getInstance().get(Calendar.MONTH) != 0) {
                new ExpensesDataAsyncTask(Variables.EXPENSES_DATA, databaseHandler.getAllLastMonthExpenses(Calendar.getInstance().get(Calendar.MONTH) - 1, Calendar.getInstance().get(Calendar.YEAR)), ExpensesActivity.this).execute();
                AppPreferences.setMonthWhenDataSent(ExpensesActivity.this, Calendar.getInstance().get(Calendar.MONTH));
            } else if (Calendar.getInstance().get(Calendar.MONTH) == 0) {
                new ExpensesDataAsyncTask(Variables.EXPENSES_DATA, databaseHandler.getAllLastMonthExpenses(11, Calendar.getInstance().get(Calendar.YEAR - 1)), ExpensesActivity.this).execute();
                AppPreferences.setMonthWhenDataSent(ExpensesActivity.this, Calendar.getInstance().get(Calendar.MONTH));
            }
        }


        expensesAll = databaseHandler.getAllExpenses();

        /*
        Text field for sum of expenses
        Text field for category sums
        Mapping the expenses...
        ...check expensesToMap() for more info
         */
        sumOfExpenses = findViewById(R.id.all_expenses_sum);
        sumsOfCategories = findViewById(R.id.all_expenses_by_cate);
        categoryExpenses = new HashMap<>();
        expensesToMap();

        /*
        Adding the data from categoryExpenses to two lists for the pie chart
         */
        for (String i : categoryExpenses.keySet()) {
            yData.add(categoryExpenses.get(i));
            xData.add(i);
        }

        FloatingActionButton addExpense = findViewById(R.id.fab2);
        addExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ExpensesActivity.this, CreateExpenseActivity.class);
                intent.putExtra("expense", "new");
                startActivity(intent);
                finish();
            }
        });
        Button allExpenses = findViewById(R.id.allexpenses);
        allExpenses.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expensesAll.size() >= 1) {
                    Intent intent = new Intent(ExpensesActivity.this, AllExpensesActivity.class);
                    intent.putExtra("expense", "new");
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), getResources().getText(R.string.noexpenses), Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (expensesAll.size() >= 1) {
            sumC = "";
            sum = "Total sum: " + databaseHandler.getSumOfExpenses();
            sumOfExpenses.setText(sum);
            expensesPieChart();
            for (String i : categoryExpenses.keySet()) {
                sumC = sumC + i + " " + categoryExpenses.get(i).toString() + "; ";
            }
            sumsOfCategories.setText(sumC);
        }


        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavView_Bar);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(2);
        menuItem.setChecked(true);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.reminders:
                        Intent intent0 = new Intent(ExpensesActivity.this, MainActivity.class);
                        startActivity(intent0);
                        finish();
                        break;

                    case R.id.groups:
                        if (databaseHandler.getActiveUser().getUserid().equals("local")) {
                            Toast.makeText(ExpensesActivity.this, getResources().getText(R.string.needtobelogged), Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent1 = new Intent(ExpensesActivity.this, GroupsActivity.class);
                            startActivity(intent1);
                            finish();
                        }
                        break;

                    case R.id.expenses:
                        break;

                }
                return false;
            }
        });


    }

    public void expensesPieChart() {

        pieChart = findViewById(R.id.expensepiechart);
        pieChart.invalidate();
        pieChart.setHoleColor(Color.TRANSPARENT);
        pieChart.setHoleRadius(35f);
        pieChart.setTransparentCircleAlpha(0);
        pieChart.setCenterTextSize(10);
        Description chartDesc = new Description();
        chartDesc.setText("All expenses");
        chartDesc.setTextSize(15);
        pieChart.setDescription(chartDesc);
        if (yData != null && xData != null) addDataSet();
    }

    /*
     All expenses are separately in the database
     Mapping the expenses by category, summing the amounts
      */
    public void expensesToMap() {
        for (int i = 0; i < expensesAll.size(); i++) {
            String tempKey = changeToRealCategory(expensesAll.get(i).get_typeOfExpense());

            if (categoryExpenses.containsKey(tempKey)) {
                float current = categoryExpenses.get(tempKey);
                categoryExpenses.put(tempKey, current + expensesAll.get(i).get_costOfExpense());
            } else categoryExpenses.put(tempKey, expensesAll.get(i).get_costOfExpense());

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.settings_expensesactivity, menu);
        if (databaseHandler.getActiveUser().getEmail().equals("local")) {
            menu.getItem(3).setTitle("Login");
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.settings:
                Intent intent = new Intent(ExpensesActivity.this, SettingsActivity.class);
                intent.putExtra("class", "expensesactivity");
                startActivity(intent);
                finish();
                return true;
            case R.id.logout:
                if (databaseHandler.getActiveUser().getEmail().equals("local")) {
                    startActivity(new Intent(ExpensesActivity.this, LoginActivity.class));
                    finish();
                } else {
                    databaseHandler.updateUserActivityToFalse();
                    Intent intent2 = new Intent(ExpensesActivity.this, FirstScreenActivity.class);
                    startActivity(intent2);
                    finish();
                }
                return true;
        }

        return true;
    }

    public String changeToRealCategory(String letter) {
        switch (letter) {
            case "f":
                return getResources().getString(R.string.food);
            case "t":
                return getResources().getString(R.string.transport);
            case "e":
                return getResources().getString(R.string.entertainment);
            case "c":
                return getResources().getString(R.string.clothes);
            case "o":
                return getResources().getString(R.string.other);
        }
        return "ERROR";
    }

    private void addDataSet() {
        ArrayList<PieEntry> yEntrys = new ArrayList<>();

        for(int i = 0; i < yData.size(); i++){
            yEntrys.add(new PieEntry(yData.get(i), xData.get(i)));
        }

        //create the data set
        PieDataSet pieDataSet = new PieDataSet(yEntrys, "");
        pieDataSet.setSliceSpace(2);
        pieDataSet.setValueTextSize(12);


        //add colors to dataset
        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(Color.GRAY);
        colors.add(Color.BLUE);
        colors.add(Color.RED);
        colors.add(Color.GREEN);
        colors.add(Color.MAGENTA);

        pieDataSet.setColors(colors);

        Legend legend = pieChart.getLegend();
        legend.setForm(Legend.LegendForm.CIRCLE);


        //create pie data object
        PieData pieData = new PieData(pieDataSet);

        pieChart.setData(pieData);

        pieChart.invalidate();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private static class ExpensesDataAsyncTask extends AsyncTask<Void, Void, Void> {

        private WeakReference<ExpensesActivity> activityReference;

        private String API_URL;
        private List<Expense> expenses;

        ExpensesDataAsyncTask(String API_URL, List<Expense> expenses, ExpensesActivity context) {
            this.API_URL = API_URL;
            this.expenses = expenses;
            activityReference = new WeakReference<>(context);
        }

        protected Void doInBackground(Void... voids) {
            try {
                URL url = new URL(API_URL);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json");
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setConnectTimeout(5000);

                JSONObject jsonParam = new JSONObject();
                jsonParam.put("head", "expenses");
                jsonParam.put("expensessize", String.valueOf(expenses.size()));

                for (int i = 0; i < expenses.size(); i++) {
                    JSONArray jArray3 = new JSONArray();
                    Expense expense = expenses.get(i);
                    jArray3.put(expense.get_typeOfExpense());
                    jArray3.put(expense.get_costOfExpense());
                    jArray3.put(expense.get_dateOfCreation());
                    jsonParam.put(String.valueOf(i), jArray3);
                }

                Log.i("JSON", jsonParam.toString());

                DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                os.writeBytes(jsonParam.toString());

                os.flush();
                os.close();

                final String result = IOUtils.toString(conn.getInputStream(), StandardCharsets.UTF_8);

                Log.i("STATUS", String.valueOf(conn.getResponseCode()));
                Log.i("MSG", conn.getResponseMessage());
                Log.i("RESPONSE", result);
                conn.disconnect();

            } catch (Exception e) {
                e.printStackTrace();

            }
            return null;
        }

//        @Override
//        protected void onPostExecute(Integer result) {
//
//        }
    }

}
