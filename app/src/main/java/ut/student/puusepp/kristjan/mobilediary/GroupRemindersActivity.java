package ut.student.puusepp.kristjan.mobilediary;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class GroupRemindersActivity extends AppCompatActivity {

    ArrayList<GroupReminder> groupReminders = new ArrayList<>();
    ListView listView;
    GroupRemindersAdapter groupRemindersAdapter;
    DatabaseHandler databaseHandler = new DatabaseHandler(this);
    String groupId;
    Context context;
    SwipeRefreshLayout mySwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_reminders);
        context = this;

        Intent intent = getIntent();
        groupId = intent.getStringExtra("groupid");

        listView = findViewById(R.id.listOfGroupReminders);
        groupRemindersAdapter = new GroupRemindersAdapter(this, R.layout.group_reminder_item, groupReminders);
        listView.setAdapter(groupRemindersAdapter);

        new GetGroupRemindersAsyncTask(this, databaseHandler.getActiveUser().getUserid(), groupId, Variables.GET_GROUP_REMINDERS).execute();

        mySwipeRefreshLayout = findViewById(R.id.swiperefreshgroupreminders);
        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        new GetGroupRemindersAsyncTask(GroupRemindersActivity.this, databaseHandler.getActiveUser().getUserid(), groupId, Variables.GET_GROUP_REMINDERS).execute();
                    }
                }
        );

        final FloatingActionButton createNewGroupReminder = findViewById(R.id.addGroupReminder);
        createNewGroupReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(GroupRemindersActivity.this, CreateReminderActivity.class);
                intent.putExtra("ce", "gcreate");
                intent.putExtra("groupid", groupId);
                startActivity(intent);
                finish();
            }
        });

    }

    private class GroupRemindersAdapter extends ArrayAdapter<GroupReminder> {
        private int layout;
        private List<GroupReminder> groupReminders;

        public GroupRemindersAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<GroupReminder> _groupReminders) {
            super(context, resource, _groupReminders);
            layout = resource;
            groupReminders = _groupReminders;
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull final ViewGroup parent) {
            ViewHolder mainViewHolder;
            if (convertView == null) {

                final LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(layout, parent, false);

                final ViewHolder viewHolder = new ViewHolder();

                viewHolder.groupReminderTitle = convertView.findViewById(R.id.groupReminderTitle);
                viewHolder.groupReminderTitle.setText(groupReminders.get(position).get_titleOfReminder());

                viewHolder.groupReminderEnddate = convertView.findViewById(R.id.groupReminderEnddate);
                if (groupReminders.get(position).get_dateOfExpiration() != null && groupReminders.get(position).get_timeOfExpiration() != null) {
                    String endDateTime = groupReminders.get(position).get_dateOfExpiration() + "  " + groupReminders.get(position).get_timeOfExpiration();
                    viewHolder.groupReminderEnddate.setText(endDateTime);
                }

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(GroupRemindersActivity.this, CreateReminderActivity.class);
                        intent.putExtra("ce", "gtrue");
                        intent.putExtra("groupid", groupId);
                        intent.putExtra("reminderid", groupReminders.get(position).getReminderId());
                        intent.putExtra("title", groupReminders.get(position).get_titleOfReminder());
                        intent.putExtra("content", groupReminders.get(position).get_contentOfReminder());
                        intent.putExtra("createddate", groupReminders.get(position).get_dateOfCreation());
                        intent.putExtra("enddate", groupReminders.get(position).get_dateOfExpiration());
                        intent.putExtra("endtime", groupReminders.get(position).get_timeOfExpiration());

                        startActivity(intent);
                        finish();
                    }
                });
                convertView.setTag(viewHolder);

            }
            else {

                mainViewHolder = (GroupRemindersActivity.ViewHolder) convertView.getTag();
                mainViewHolder.groupReminderTitle.setText(getItem(position).get_titleOfReminder());

            }
            //return super.getView(position, convertView, parent);
            return convertView;
        }
    }

    public class ViewHolder {
        TextView groupReminderTitle;
        TextView groupReminderEnddate;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(GroupRemindersActivity.this, GroupsActivity.class));
        finish();
    }

    private static class GetGroupRemindersAsyncTask extends AsyncTask<Void, Void, Integer> {

        private WeakReference<GroupRemindersActivity> activityReference;
        private String userId;
        private String groupId;
        private String API_URL;

        GetGroupRemindersAsyncTask(GroupRemindersActivity context, String userId, String groupId, String API_URL) {
            activityReference = new WeakReference<>(context);
            this.userId = userId;
            this.groupId = groupId;
            this.API_URL = API_URL;

        }

        protected Integer doInBackground(Void... voids) {
            try {
                GroupRemindersActivity groupRemindersActivity = activityReference.get();
                    URL url = new URL(API_URL);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("POST");
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                    conn.setRequestProperty("Accept", "application/json");
                    conn.setDoOutput(true);
                    conn.setDoInput(true);

                    JSONObject jsonParam = new JSONObject();
                    jsonParam.put("head", "getgroupreminders");
                    jsonParam.put("userid", userId);
                    jsonParam.put("groupid", groupId);

                    Log.i("JSON", jsonParam.toString());

                    DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                    //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                    os.writeBytes(jsonParam.toString());

                    os.flush();
                    os.close();

                    final String result = IOUtils.toString(conn.getInputStream(), StandardCharsets.UTF_8);
                    groupRemindersActivity.groupReminders.clear();
                    JSONObject jsonresult = new JSONObject(result);
                    for (int i = 1; i < Integer.parseInt(jsonresult.getString("0")) + 1; i++) {
                        JSONArray ja = jsonresult.getJSONArray(String.valueOf(i));
                        groupRemindersActivity.groupReminders.add(new GroupReminder(ja.getString(1), ja.getString(0), ja.getString(2), ja.getString(3), ja.getString(4), ja.getString(5), ja.getString(6)));
                    }


                    Log.i("STATUS", String.valueOf(conn.getResponseCode()));
                    Log.i("MSG", conn.getResponseMessage());
                    Log.i("RESPONSE", result);
                    conn.disconnect();
                    return 0;
                } catch (Exception e) {
                    e.printStackTrace();
                    return 1;
                }

        }

        @Override
        protected void onPostExecute(Integer result) {
            GroupRemindersActivity groupRemindersActivity = activityReference.get();
            if (result == 0) {
                groupRemindersActivity.mySwipeRefreshLayout.setRefreshing(false);
                groupRemindersActivity.groupRemindersAdapter.notifyDataSetChanged();
            }
            if (result == 1) {
                groupRemindersActivity.mySwipeRefreshLayout.setRefreshing(false);
                Toast.makeText(groupRemindersActivity, "Connection lost", Toast.LENGTH_LONG).show();
            }
        }
    }
}
