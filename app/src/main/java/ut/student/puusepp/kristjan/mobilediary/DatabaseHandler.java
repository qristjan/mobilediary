package ut.student.puusepp.kristjan.mobilediary;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "mobilediary";

    private static final String KEY_ID = "id";

    private static final String TABLE_REMINDERS = "reminders";
    private static final String KEY_TITLE = "title";
    private static final String KEY_CONTENT = "content";
    private static final String KEY_DATE = "date";
    private static final String KEY_ENDDATE = "enddate";
    private static final String KEY_ENDTIME = "endtime";
    private static final String KEY_COMPLETED = "status";
    private static final String KEY_USERID = "userid";
    private static final String KEY_SERVERID = "sid";

    private static final String TABLE_REMINDER_STATE = "reminderstate";
    private static final String KEY_STATE = "state";
    private static final String KEY_REMINDER_SERVERID = "serverid";
    private static final String KEY_STATE_TIME = "statetime";

    private static final String TABLE_EXPENSES = "expenses";
    private static final String KEY_TYPE_OF_EXPENSE = "expensetype";
    private static final String KEY_DESC_OF_EXPENSE = "expensedesc";
    private static final String KEY_COST_OF_EXPENSE = "expensecost";
    private static final String KEY_DATE_ADDED = "dateadded";

    private static final String TABLE_USER = "user";
    private static final String KEY_USER_ID = "userid";
    private static final String KEY_USER_SID = "usersid";
    private static final String KEY_USER_EMAIL = "email";
    private static final String KEY_USER_ACTIVE = "active";

    private static final String TABLE_SETTINGS = "settings";
    private static final String KEY_SETTINGS_SHOWCOMPLETED = "showcompleted";
    private static final String KEY_SETTINGS_LOCALE = "locale";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_REMINDERS_TABLE = "CREATE TABLE " + TABLE_REMINDERS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_TITLE + " TEXT," + KEY_CONTENT + " TEXT,"
                + KEY_DATE + " TEXT," + KEY_ENDDATE + " TEXT," + KEY_ENDTIME + " TEXT," + KEY_COMPLETED + " TEXT," + KEY_USERID + " TEXT," + KEY_SERVERID + " TEXT" + ")";

        String CREATE_EXPENSES_TABLE = "CREATE TABLE " + TABLE_EXPENSES + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_TYPE_OF_EXPENSE + " TEXT," + KEY_DESC_OF_EXPENSE + " TEXT," + KEY_COST_OF_EXPENSE + " REAL,"
                + KEY_DATE_ADDED + " TEXT" + ")";

        String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER + "("
                + KEY_USER_ID + " INTEGER PRIMARY KEY," + KEY_USER_SID + " TEXT," + KEY_USER_EMAIL + " TEXT," + KEY_USER_ACTIVE + " TEXT" + ")";

        String CREATE_SETTINGS_TABLE = "CREATE TABLE " + TABLE_SETTINGS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_SETTINGS_SHOWCOMPLETED + " INTEGER," + KEY_SETTINGS_LOCALE + " TEXT" + ")";

        String CREATE_REMSTAT_TABLE = "CREATE TABLE " + TABLE_REMINDER_STATE + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_STATE + " INTEGER, " + KEY_STATE_TIME + " TEXT, " + KEY_REMINDER_SERVERID + " TEXT" + ")";

        db.execSQL(CREATE_REMINDERS_TABLE);
        db.execSQL(CREATE_EXPENSES_TABLE);
        db.execSQL(CREATE_USER_TABLE);
        db.execSQL(CREATE_SETTINGS_TABLE);
        db.execSQL(CREATE_REMSTAT_TABLE);

        String selectQuery1 = "INSERT INTO " + TABLE_SETTINGS + "(" + KEY_SETTINGS_SHOWCOMPLETED + ", " + KEY_SETTINGS_LOCALE + ")" + " VALUES(0, \"en\")";
        db.execSQL(selectQuery1);
    }

    //String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_REMINDERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXPENSES);
        // Create tables again
        onCreate(db);
    }

    public void addUser(User user) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_USER_SID, user.getUserid());
        values.put(KEY_USER_EMAIL, user.getEmail());
        values.put(KEY_USER_ACTIVE, user.getActive());

        db.insert(TABLE_USER, null, values);
        db.close();

    }

    public int getShowCompleted() {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT " + KEY_SETTINGS_SHOWCOMPLETED + " FROM " + TABLE_SETTINGS;
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        int s = cursor.getInt(0);
        cursor.close();
        db.close();
        return s;
    }

    public String getCurrentLang() {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT " + KEY_SETTINGS_LOCALE + " FROM " + TABLE_SETTINGS;
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        String s = cursor.getString(0);
        cursor.close();
        db.close();
        return s;
    }


    public void changeSettingsLocale(String locale) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "UPDATE " + TABLE_SETTINGS + " SET " + KEY_SETTINGS_LOCALE + "='" + locale + "'";
        db.execSQL(selectQuery);
        db.close();
    }

    public void changeShowCompletedToFalse() {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "UPDATE " + TABLE_SETTINGS + " SET " + KEY_SETTINGS_SHOWCOMPLETED + "=1";
        db.execSQL(selectQuery);
        db.close();
    }

    public void changeShowCompletedToTrue() {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "UPDATE " + TABLE_SETTINGS + " SET " + KEY_SETTINGS_SHOWCOMPLETED + "=0    ";
        db.execSQL(selectQuery);
        db.close();
    }

    public List<String> getAllUsersEmails() {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT email FROM " + TABLE_USER;
        Cursor cursor = db.rawQuery(selectQuery, null);
        List<String> emails = new ArrayList<>();

        if (cursor.moveToFirst() && cursor.getCount() >= 1) {
            do {
                emails.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return emails;
    }

    public User getActiveUser() {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT " + KEY_USER_ID + "," + KEY_USER_SID + "," + KEY_USER_EMAIL + "," + KEY_USER_ACTIVE + " FROM " + TABLE_USER + " where active='t'";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor != null && cursor.getCount() >= 1) {
            cursor.moveToFirst();
            User user = new User(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));
            cursor.close();

            return user;
        }
        else {

            return null;
        }
    }

    public void updateUserActivityToFalse() {
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "UPDATE " + TABLE_USER + " SET " + KEY_USER_ACTIVE + "='f' WHERE " + KEY_USER_ACTIVE + "='t'";
        db.execSQL(selectQuery);
        db.close();
    }

    public void updateUserActivityToTrue(String email) {
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "UPDATE " + TABLE_USER + " SET " + KEY_USER_ACTIVE + "='t' WHERE " + KEY_USER_EMAIL + "='" + email + "'";
        db.execSQL(selectQuery);
        db.close();
    }

    /*
    State 0 = deleted
    State 1 = changed
     */
    public void addToRemStat(String id, Integer state) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_REMINDER_SERVERID, id);
        values.put(KEY_STATE, state);
        db.insert(TABLE_REMINDER_STATE, null, values);
        db.close();
    }

    public ArrayList<String> getDeletedPersonalRemindersSIDs() {
        ArrayList<String> temp = new ArrayList<>();
        String selectQuery = "SELECT " + KEY_REMINDER_SERVERID + " FROM " + TABLE_REMINDER_STATE + " where " + KEY_STATE+ "=0";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                temp.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return temp;
    }


    public void addReminder(Reminder reminder) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, reminder.get_titleOfReminder());
        values.put(KEY_CONTENT, reminder.get_contentOfReminder());
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Calendar.getInstance().getTime());
        //values.put(KEY_DATE, Calendar.getInstance().getTime().toString());
        values.put(KEY_DATE, timeStamp);
        values.put(KEY_ENDDATE, reminder.get_dateOfExpiration());
        values.put(KEY_ENDTIME, reminder.get_timeOfExpiration());
        values.put(KEY_COMPLETED, "F");
        values.put(KEY_USERID, reminder.get_userid());
        values.put(KEY_SERVERID, reminder.get_sid());
        // Inserting Row
        db.insert(TABLE_REMINDERS, null, values);
        db.close(); // Closing database connection
    }

    public void addReminderSync(Reminder reminder) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, reminder.get_titleOfReminder());
        values.put(KEY_CONTENT, reminder.get_contentOfReminder());
        values.put(KEY_DATE, reminder.get_dateOfCreation());
        values.put(KEY_ENDDATE, reminder.get_dateOfExpiration());
        values.put(KEY_ENDTIME, reminder.get_timeOfExpiration());
        values.put(KEY_COMPLETED, reminder.is_reminderCompleted());
        values.put(KEY_USERID, reminder.get_userid());
        values.put(KEY_SERVERID, reminder.get_sid());
        // Inserting Row
        db.insert(TABLE_REMINDERS, null, values);
        db.close(); // Closing database connection
    }


    public Reminder getReminder(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_REMINDERS, new String[] { KEY_ID,
                        KEY_TITLE, KEY_CONTENT, KEY_DATE, KEY_ENDDATE, KEY_ENDTIME, KEY_COMPLETED, KEY_USERID, KEY_SERVERID }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            Reminder reminder = new Reminder(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8));
            cursor.close();
            db.close();
            return reminder;
        }
        db.close();
        return null;
    }

    public String getReminderStatus(int id) {
        String result = "";
        String selectQuery = "SELECT status FROM " + TABLE_REMINDERS + " where " + KEY_ID + "=" + id;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            result = cursor.getString(0);
        }
        db.close();
        cursor.close();
        return result;
    }


    public List<Reminder> getAllReminderTitles() {

        List<Reminder> reminderTitlesList = new ArrayList<Reminder>();
        String selectQuery = "SELECT id,title,date,enddate,endtime,status,userid FROM " + TABLE_REMINDERS + " where " + KEY_USERID + "='" + getActiveUser().getUserid() + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Reminder reminder = new Reminder();
                reminder.set_id(Integer.parseInt(cursor.getString(0)));
                reminder.set_titleOfReminder(cursor.getString(1));
                reminder.set_dateOfCreation(cursor.getString(2));
                reminder.set_dateOfExpiration(cursor.getString(3));
                reminder.set_timeOfExpiration(cursor.getString(4));
                reminder.set_reminderCompleted(cursor.getString(5));
                reminder.set_userid(cursor.getString(6));
                reminderTitlesList.add(reminder);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return reminderTitlesList;
    }

    public List<Reminder> getAllReminders() {

        List<Reminder> remindersList = new ArrayList<Reminder>();
        String selectQuery = "SELECT * FROM " + TABLE_REMINDERS + " where " + KEY_USERID + "='" + getActiveUser().getUserid() + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Reminder reminder = new Reminder();
                reminder.set_id(Integer.parseInt(cursor.getString(0)));
                reminder.set_titleOfReminder(cursor.getString(1));
                reminder.set_contentOfReminder(cursor.getString(2));
                reminder.set_dateOfCreation(cursor.getString(3));
                reminder.set_dateOfExpiration(cursor.getString(4));
                reminder.set_timeOfExpiration(cursor.getString(5));
                reminder.set_reminderCompleted(cursor.getString(6));
                reminder.set_userid(cursor.getString(7));
                reminder.set_sid(cursor.getString(8));
                remindersList.add(reminder);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return remindersList;
    }

    public List<Reminder> getAllIncompletedReminders() {
        List<Reminder> reminderList = new ArrayList<Reminder>();
        // Select All Query
        String selectQuery = "SELECT id,title,date,enddate,endtime,status,userid,sid FROM " + TABLE_REMINDERS + " WHERE " + KEY_COMPLETED + "='F' and " + KEY_USERID + "='" + getActiveUser().getUserid() + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Reminder reminder = new Reminder();
                reminder.set_id(cursor.getInt(0));
                reminder.set_titleOfReminder(cursor.getString(1));
                reminder.set_dateOfCreation(cursor.getString(2));
                reminder.set_dateOfExpiration(cursor.getString(3));
                reminder.set_timeOfExpiration(cursor.getString(4));
                reminder.set_reminderCompleted(cursor.getString(5));
                reminder.set_sid(cursor.getString(7));
                reminderList.add(reminder);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return reminderList;
    }

    public List<Integer> getExistingSIDs() {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Integer> existingSIDs = new ArrayList<>();

        String selectQuery = "SELECT userid, sid FROM " + TABLE_REMINDERS + " WHERE " + KEY_SERVERID + " != 'local' and " + KEY_USERID + "='" + getActiveUser().getUserid() + "'";
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                existingSIDs.add(Integer.parseInt(cursor.getString(1)));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return existingSIDs;
    }

    public List<Reminder> getAllLocalReminders() {
        List<Reminder> reminderList = new ArrayList<Reminder>();
        // Select All Query
        String selectQuery = "SELECT id, title, content, date, enddate, endtime, status, userid, sid FROM " + TABLE_REMINDERS + " WHERE " + KEY_SERVERID + "='local' AND " + KEY_USERID + "='" + getActiveUser().getUserid() + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Reminder reminder = new Reminder();
                reminder.set_id(Integer.parseInt(cursor.getString(0)));
                reminder.set_titleOfReminder(cursor.getString(1));
                reminder.set_contentOfReminder(cursor.getString(2));
                reminder.set_dateOfCreation(cursor.getString(3));
                reminder.set_dateOfExpiration(cursor.getString(4));
                reminder.set_timeOfExpiration(cursor.getString(5));
                reminder.set_reminderCompleted(cursor.getString(6));
                reminder.set_userid(cursor.getString(7));
                reminder.set_sid(cursor.getString(8));
                reminderList.add(reminder);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return reminderList;
    }

    public void updateReminder(Reminder reminder) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, reminder.get_titleOfReminder());
        values.put(KEY_CONTENT, reminder.get_contentOfReminder());
        values.put(KEY_DATE, reminder.get_dateOfCreation());
        values.put(KEY_ENDDATE, reminder.get_dateOfExpiration());
        values.put(KEY_ENDTIME, reminder.get_timeOfExpiration());
        values.put(KEY_SERVERID, reminder.get_sid());
        // updating row
        db.update(TABLE_REMINDERS, values, KEY_ID + " = ?", new String[] { String.valueOf(reminder.get_id())});
        db.close();
    }

    public void updateReminderToChanged(int id) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_SERVERID, "local");
        // updating row
        db.update(TABLE_REMINDERS, values, KEY_ID + " = ?", new String[] { String.valueOf(id) });
        db.close();
    }




    public void deleteReminderById(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_REMINDERS, KEY_ID + " = ?",
                new String[] { id });
        db.close();
    }

    public void deleteReminderBySID(String sid) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_REMINDERS, KEY_SERVERID + " = ?",
                new String[] { sid });
        db.close();
    }

    public void deleteReminder(Reminder reminder) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_REMINDERS, KEY_ID + " = ?",
                new String[] { String.valueOf(reminder.get_id()) });
        db.close();
    }


    public void updateReminderStatus(Reminder reminder) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        if (reminder.is_reminderCompleted().equals("F")) {
            values.put(KEY_COMPLETED, "T");
        }
        if (reminder.is_reminderCompleted().equals("T")) {
            values.put(KEY_COMPLETED, "F");
        }

        // updating row
        db.update(TABLE_REMINDERS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(reminder.get_id())});
        db.close();
    }

    public void updateReminderSID(String reminderId, String sid) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_SERVERID, sid);
        // updating row
        db.update(TABLE_REMINDERS, values, KEY_ID + " = ?",
                new String[] { reminderId });
        db.close();
    }

    public List<String> getAllExpenseCategories() {
        List<String> expenseCategoriesList = new ArrayList<>();

        String selectQuery = "SELECT expensetype FROM " + TABLE_EXPENSES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String temp = cursor.getString(1);
                expenseCategoriesList.add(temp);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return expenseCategoriesList;
    }


    public void addExpense(Expense expense) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TYPE_OF_EXPENSE, expense.get_typeOfExpense());
        values.put(KEY_DESC_OF_EXPENSE, expense.get_descriptionOfExpense());
        values.put(KEY_COST_OF_EXPENSE, expense.get_costOfExpense());
        String timeStamp = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(Calendar.getInstance().getTime());
        values.put(KEY_DATE_ADDED, timeStamp);
        // Inserting Row
        db.insert(TABLE_EXPENSES, null, values);
        db.close(); // Closing database connection
    }

    public Expense getExpense(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_EXPENSES, new String[] { KEY_ID,
                        KEY_TYPE_OF_EXPENSE, KEY_DESC_OF_EXPENSE, KEY_COST_OF_EXPENSE, KEY_DATE_ADDED }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Expense expense = new Expense(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getFloat(3), cursor.getString(4));
        // return contact
        cursor.close();
        db.close();
        return expense;
    }

    public List<Expense> getAllExpenses() {
        List<Expense> expensesList = new ArrayList<Expense>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EXPENSES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Expense expense = new Expense();
                expense.set_id(Integer.parseInt(cursor.getString(0)));
                expense.set_typeOfExpense(cursor.getString(1));
                expense.set_descriptionOfExpense(cursor.getString(2));
                expense.set_costOfExpense(cursor.getFloat(3));
                expense.set_dateOfCreation(cursor.getString(4));
                expensesList.add(expense);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return expensesList;
    }

    public List<Expense> getAllLastMonthExpenses(int month, int year) {
        List<Expense> expensesList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_EXPENSES;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                String[] timeStamp = cursor.getString(4).split("\\.");
                if(Integer.parseInt(timeStamp[1]) == month && Integer.parseInt(timeStamp[2]) == year) {
                    Expense expense = new Expense();
                    expense.set_id(Integer.parseInt(cursor.getString(0)));
                    expense.set_typeOfExpense(cursor.getString(1));
                    expense.set_costOfExpense(cursor.getFloat(3));
                    expense.set_dateOfCreation(cursor.getString(4));
                    expensesList.add(expense);
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return expensesList;
    }



    public void updateExpense(Expense expense) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TYPE_OF_EXPENSE, expense.get_typeOfExpense());
        values.put(KEY_DESC_OF_EXPENSE, expense.get_descriptionOfExpense());
        values.put(KEY_COST_OF_EXPENSE, expense.get_costOfExpense());
        values.put(KEY_DATE_ADDED, expense.get_dateOfCreation());

        // updating row
        db.update(TABLE_EXPENSES, values, KEY_ID + " = ?",
                new String[] { String.valueOf(expense.get_id())});
        db.close();
    }

    public float getSumOfExpenses() {
        float sum = 0;

        String selectQuery = "SELECT expensecost FROM " + TABLE_EXPENSES;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                sum += cursor.getFloat(0);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return sum;
    }

    public void deleteExpense(Expense expense) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_EXPENSES, KEY_ID + " = ?",
                new String[] { String.valueOf(expense.get_id()) });
        db.close();
    }

}