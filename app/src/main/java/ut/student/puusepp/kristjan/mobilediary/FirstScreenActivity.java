package ut.student.puusepp.kristjan.mobilediary;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;

import java.util.Locale;

public class FirstScreenActivity extends AppCompatActivity {

    DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        db = new DatabaseHandler(this);
        Locale myLocale = new Locale(db.getCurrentLang());
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firstscreen);


        if (db.getActiveUser() != null) {
            Intent intent = new Intent(FirstScreenActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }

        Button withoutSignIn = findViewById(R.id.withoutSignin);
        withoutSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (db.getActiveUser() != null) {
                    if (db.getActiveUser().getEmail().equals("local")) {
                        db.updateUserActivityToFalse();
                        db.updateUserActivityToTrue("local");
                    }
                } else {
                    db.updateUserActivityToFalse();
                    db.addUser(new User("local", "local", "t"));
                }
                Intent intent = new Intent(FirstScreenActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        Button goToRegister = findViewById(R.id.login2register);
        goToRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FirstScreenActivity.this, RegisterActivity.class);
                startActivity(intent);
                finish();
            }
        });

        Button goToLogin = findViewById(R.id.login2logging);
        goToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(FirstScreenActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
