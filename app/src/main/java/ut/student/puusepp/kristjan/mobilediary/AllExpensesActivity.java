package ut.student.puusepp.kristjan.mobilediary;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

public class AllExpensesActivity extends AppCompatActivity {

    List<Expense> expenses;
    ListView listView;
    ExpensesListAdapter expensesListAdapter;
    DatabaseHandler databaseHandler;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_expenses);

        databaseHandler = new DatabaseHandler(this);
        expenses = databaseHandler.getAllExpenses();
        listView = findViewById(R.id.listOfExpenses);
        expensesListAdapter = new ExpensesListAdapter(this, R.layout.expense_item, expenses);
        listView.setAdapter(expensesListAdapter);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(AllExpensesActivity.this, ExpensesActivity.class);
        startActivity(intent);
    }

    private class ExpensesListAdapter extends ArrayAdapter<Expense> {
        private int layout;
        private List<Expense> expenses;
        public ExpensesListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Expense> _expenses) {
            super(context, resource, _expenses);
            layout = resource;
            expenses = _expenses;
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull final ViewGroup parent) {

            if (convertView == null) {
                final LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(layout, parent, false);

                final ViewHolder viewHolder = new ViewHolder();

                viewHolder.title = convertView.findViewById(R.id.list_item_expense_cate);
                viewHolder.title.setText(expenses.get(position).toString());

                viewHolder.date = convertView.findViewById(R.id.list_item_expense_date);
                viewHolder.date.setText(expenses.get(position).get_dateOfCreation());

                viewHolder.imgbutton = convertView.findViewById(R.id.list_item_trash_button);
                viewHolder.imgbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        databaseHandler.deleteExpense(expenses.get(position));
                        if (expenses.size() > 1) recreate();
                        else startActivity(new Intent(AllExpensesActivity.this, MainActivity.class));
                    }
                });

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(AllExpensesActivity.this, CreateExpenseActivity.class);
                        intent.putExtra("expense", "existing");
                        intent.putExtra("id", expenses.get(position).get_id());
                        intent.putExtra("cost", expenses.get(position).get_costOfExpense());
                        intent.putExtra("date", expenses.get(position).get_dateOfCreation());
                        intent.putExtra("type", expenses.get(position).get_typeOfExpense());
                        intent.putExtra("description", expenses.get(position).get_descriptionOfExpense());
                        startActivity(intent);
                        finish();
                    }
                });

                convertView.setTag(viewHolder);
            }

            return convertView;
        }
    }

    public class ViewHolder {
        TextView title;
        TextView date;
        ImageButton imgbutton;
    }
}
