package ut.student.puusepp.kristjan.mobilediary;

public class GroupReminder {

    private String groupId;
    private String reminderId;
    private String _titleOfReminder;
    private String _contentOfReminder;
    private String _dateOfCreation;
    private String _dateOfExpiration;
    private String _timeOfExpiration;

    public GroupReminder(String groupId, String reminderId, String _titleOfReminder, String _contentOfReminder, String _dateOfCreation, String _dateOfExpiration, String _timeOfExpiration) {
        this.groupId = groupId;
        this.reminderId = reminderId;
        this._titleOfReminder = _titleOfReminder;
        this._contentOfReminder = _contentOfReminder;
        this._dateOfCreation = _dateOfCreation;
        this._dateOfExpiration = _dateOfExpiration;
        this._timeOfExpiration = _timeOfExpiration;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getReminderId() {
        return reminderId;
    }

    public void setReminderId(String reminderId) {
        this.reminderId = reminderId;
    }

    public String get_titleOfReminder() {
        return _titleOfReminder;
    }

    public void set_titleOfReminder(String _titleOfReminder) {
        this._titleOfReminder = _titleOfReminder;
    }

    public String get_contentOfReminder() {
        return _contentOfReminder;
    }

    public void set_contentOfReminder(String _contentOfReminder) {
        this._contentOfReminder = _contentOfReminder;
    }

    public String get_dateOfCreation() {
        return _dateOfCreation;
    }

    public void set_dateOfCreation(String _dateOfCreation) {
        this._dateOfCreation = _dateOfCreation;
    }

    public String get_dateOfExpiration() {
        return _dateOfExpiration;
    }

    public void set_dateOfExpiration(String _dateOfExpiration) {
        this._dateOfExpiration = _dateOfExpiration;
    }

    public String get_timeOfExpiration() {
        return _timeOfExpiration;
    }

    public void set_timeOfExpiration(String _timeOfExpiration) {
        this._timeOfExpiration = _timeOfExpiration;
    }
}
