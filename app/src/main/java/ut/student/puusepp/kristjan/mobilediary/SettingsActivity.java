package ut.student.puusepp.kristjan.mobilediary;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import java.util.Locale;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;

public class SettingsActivity extends AppCompatActivity {

    DatabaseHandler databaseHandler;
    String lastClass = "mainactivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        databaseHandler = new DatabaseHandler(this);
        lastClass = getIntent().getStringExtra("class");
        Button languageButton = findViewById(R.id.btnChangeLang);
        languageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(SettingsActivity.this);
                dialog.setContentView(R.layout.dialog_languages);

                final Button btn = dialog.findViewById(R.id.est);
                final Button btn2 = dialog.findViewById(R.id.eng);

                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        setLocale("et");
                    }
                });

                btn2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        setLocale("en");
                    }
                });

                dialog.show();
            }
        });

    }
    @Override
    public void onBackPressed() {
        switch (lastClass) {
            case "expensesactivity":
                startActivity(new Intent(SettingsActivity.this, ExpensesActivity.class));
                finish();
                break;
            case "mainactivity":
                startActivity(new Intent(SettingsActivity.this, MainActivity.class));
                finish();
                break;
        }
    }

    //https://stackoverflow.com/questions/12908289/how-to-change-language-of-app-when-user-selects-language
    public void setLocale(String lang) {
        databaseHandler.changeSettingsLocale(lang);
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
        Intent refresh = new Intent(this, SettingsActivity.class);
        startActivity(refresh);
        finish();
    }
}
