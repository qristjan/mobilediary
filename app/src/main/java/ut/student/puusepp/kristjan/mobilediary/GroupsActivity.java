package ut.student.puusepp.kristjan.mobilediary;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.icu.util.ValueIterator;
import android.os.AsyncTask;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class GroupsActivity extends AppCompatActivity {

    List<Group> myGroups = new ArrayList<>();
    ListView groupsListView;
    GroupsListAdapter groupsListAdapter;
    EditText createGroupName;
    SwipeRefreshLayout mySwipeRefreshLayout;

    DatabaseHandler databaseHandler = new DatabaseHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_groups);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottomNavView_Bar);
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);

        groupsListView = findViewById(R.id.listOfGroups);
        groupsListAdapter = new GroupsListAdapter(this, R.layout.group_item, myGroups);
        groupsListView.setAdapter(groupsListAdapter);

        new GroupsAsyncTask(this, 0, Variables.GET_GROUPS).execute();

        mySwipeRefreshLayout = findViewById(R.id.swiperefreshgroups);
        mySwipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        new GroupsAsyncTask(GroupsActivity.this, 0, Variables.GET_GROUPS).execute();
                    }
                }
        );

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.reminders:
                        Intent intent0 = new Intent(GroupsActivity.this, MainActivity.class);
                        startActivity(intent0);
                        finish();
                        break;

                    case R.id.groups:

                        break;

                    case R.id.expenses:
                        Intent intent2 = new Intent(GroupsActivity.this, ExpensesActivity.class);
                        startActivity(intent2);
                        finish();
                        break;
                }
                return false;
            }
        });

        final FloatingActionButton createNewGroup = findViewById(R.id.createGroupButton);
            createNewGroup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        final Dialog dialog = new Dialog(GroupsActivity.this);
                        dialog.setContentView(R.layout.dialog_creategroup);

                        createGroupName = dialog.findViewById(R.id.createGroupName);
                        final Button saveGroupButton = dialog.findViewById(R.id.saveGroupButton);

                        saveGroupButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                new GroupsAsyncTask(GroupsActivity.this, 1, Variables.ADD_NEW_GROUP).execute();
                                dialog.dismiss();
                            }
                        });

                        dialog.show();
                    }
            });


    }

    private class GroupsListAdapter extends ArrayAdapter<Group> {
        private int layout;
        private List<Group> myGroups;

        public GroupsListAdapter(@NonNull Context context, @LayoutRes int resource, @NonNull List<Group> _myGroups) {
            super(context, resource, _myGroups);
            layout = resource;
            myGroups = _myGroups;
        }

        @NonNull
        @Override
        public View getView(final int position, @Nullable View convertView, @NonNull final ViewGroup parent) {
            ViewHolder2 mainViewHolder;
            if (convertView == null) {

                final LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(layout, parent, false);

                final ViewHolder2 viewHolder = new ViewHolder2();

                viewHolder.groupName = convertView.findViewById(R.id.list_group_item);
                viewHolder.groupName.setText(myGroups.get(position).getGroupName());

                viewHolder.groupSettingsButton = convertView.findViewById(R.id.list_group_settings);
                viewHolder.groupSettingsButton.setBackground(getDrawable(R.drawable.ic_check_black_24dp));
                viewHolder.groupSettingsButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Dialog dialog = new Dialog(GroupsActivity.this);
                        dialog.setContentView(R.layout.dialog_groupsettings);
                        dialog.findViewById(R.id.saveNewGroupName).setVisibility(View.GONE);
                        dialog.findViewById(R.id.createGroupNameField).setVisibility(View.GONE);

                        final Button btn = dialog.findViewById(R.id.groupMembers);
                        final Button btn2 = dialog.findViewById(R.id.deleteGroup);
                        final Button btn3 = dialog.findViewById(R.id.leaveGroup);
                        final Button btn4 = dialog.findViewById(R.id.changeGroupName);
                        final Button btn5 = dialog.findViewById(R.id.saveNewGroupName);

                        btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                dialog.dismiss();
                                Intent intent = new Intent(GroupsActivity.this, GroupMembersActivity.class);

                                intent.putExtra("groupId", myGroups.get(position).getServerId());
                                startActivity(intent);
                                finish();

                            }
                        });

                        btn2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                new GroupsAsyncTask(GroupsActivity.this, 2, "deletegroup", databaseHandler.getActiveUser().getUserid(), "", myGroups.get(position).getServerId(), "", Variables.GROUP_SETTINGS).execute();
                                //myGroups.remove(position);
                                //groupsListAdapter.notifyDataSetChanged();
                            }
                        });

                        btn3.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                new GroupsAsyncTask(GroupsActivity.this, 2, "leavegroup", databaseHandler.getActiveUser().getUserid(), "", myGroups.get(position).getServerId(), "", Variables.GROUP_SETTINGS).execute();
                                myGroups.remove(position);
                                groupsListAdapter.notifyDataSetChanged();
                            }
                        });

                        btn4.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.findViewById(R.id.saveNewGroupName).setVisibility(View.VISIBLE);
                                dialog.findViewById(R.id.createGroupNameField).setVisibility(View.VISIBLE);

                            }
                        });

                        btn5.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                EditText editText = dialog.findViewById(R.id.createGroupNameField);
                                //myGroups.get(position).setGroupName(editText.getText().toString());
                                //groupsListAdapter.notifyDataSetChanged();
                                new GroupsAsyncTask(GroupsActivity.this, 2, "changename", databaseHandler.getActiveUser().getUserid(), editText.getText().toString(), myGroups.get(position).getServerId(), "", Variables.GROUP_SETTINGS).execute();

                            }
                        });

                        dialog.show();
                    }
                });

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(GroupsActivity.this, GroupRemindersActivity.class);
                        intent.putExtra("groupid", myGroups.get(position).getServerId());
                        startActivity(intent);
                        finish();
                    }
                });

                convertView.setTag(viewHolder);

            }
            else {

                mainViewHolder = (ViewHolder2) convertView.getTag();
                mainViewHolder.groupName.setText(getItem(position).getGroupName());

            }
            //return super.getView(position, convertView, parent);
            return convertView;
        }
    }

    public class ViewHolder2 {
        TextView groupName;
        ImageButton groupSettingsButton;
    }

//    private void groupSettings(String task, String newGroupName, String groupId) throws InterruptedException, ExecutionException{
//        ExecutorService es2 = Executors.newSingleThreadExecutor();
//        final Future<String> result = es2.submit(new GroupSettings(databaseHandler.getActiveUser().getUserid(), task, newGroupName, groupId, ""));
//        try {
//            if (result.get().equals("deleted")) {
//                Toast.makeText(GroupsActivity.this, getResources().getText(R.string.deleted), Toast.LENGTH_SHORT).show();
//            }
//            if (result.get().equals("notadmin")) {
//                Toast.makeText(GroupsActivity.this, getResources().getText(R.string.youneedtobeadmin), Toast.LENGTH_SHORT).show();
//            }
//            if (result.get().equals("left")) {
//                Toast.makeText(GroupsActivity.this, getResources().getText(R.string.leftgroup), Toast.LENGTH_SHORT).show();
//            }
//            if (result.get().equals("changed")) {
//                Toast.makeText(GroupsActivity.this, getResources().getText(R.string.namechanged), Toast.LENGTH_SHORT).show();
//            }
//            new GroupsAsyncTask(this, 0, Variables.GET_GROUPS).execute();
//            groupsListAdapter.notifyDataSetChanged();
//        } catch (InterruptedException | ExecutionException e) {
//            Toast.makeText(GroupsActivity.this, getResources().getText(R.string.wrong), Toast.LENGTH_SHORT).show();
//            e.printStackTrace();
//        }
//        es2.shutdown();
//    }

    private static class GroupsAsyncTask extends AsyncTask<Void, Void, String> {

        private WeakReference<GroupsActivity> activityReference;
        private Integer taskNumber;
        private String taskName;
        private String API_URL;

        private String userId;
        private String newGroupName;
        private String groupId;
        private String memberEmail;

        GroupsAsyncTask(GroupsActivity context, Integer taskNumber, String API_URL) {
            activityReference = new WeakReference<>(context);
            this.taskNumber = taskNumber;
            this.API_URL = API_URL;
        }

        GroupsAsyncTask(GroupsActivity context, Integer taskNumber, String taskName, String userId, String newGroupName, String groupId, String memberEmail, String API_URL) {
            activityReference = new WeakReference<>(context);
            this.taskNumber = taskNumber;
            this.taskName = taskName;
            this.userId = userId;
            this.newGroupName = newGroupName;
            this.groupId = groupId;
            this.memberEmail = memberEmail;
            this.API_URL = API_URL;
        }

        protected String doInBackground(Void... voids) {
            GroupsActivity groupsActivity = activityReference.get();
            try {
                URL url = new URL(API_URL);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json");
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setConnectTimeout(5000);

                JSONObject jsonParam = new JSONObject();

                if (taskNumber == 0) {
                    jsonParam.put("head", "getmygroups");
                    jsonParam.put("userid", groupsActivity.databaseHandler.getActiveUser().getUserid());
                } else if (taskNumber == 1) {
                    jsonParam.put("head", "creategroup");
                    jsonParam.put("userid", groupsActivity.databaseHandler.getActiveUser().getUserid());
                    jsonParam.put("groupname", groupsActivity.createGroupName.getText().toString());
                } else if (taskNumber == 2){
                    jsonParam = createJSON(taskName, userId, newGroupName, groupId, memberEmail);
                }

                Log.i("JSON", jsonParam.toString());

                DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                os.writeBytes(jsonParam.toString());

                os.flush();
                os.close();

                final String result = IOUtils.toString(conn.getInputStream(), StandardCharsets.UTF_8);
                Log.i(Variables.TAG, result);
                if (result.equals("error")) {
                    return "1";
                }
                if (taskNumber == 0) {
                    groupsActivity.myGroups.clear();
                    JSONObject jsonresult = new JSONObject(result);
                    for (int i = 1; i < Integer.parseInt(jsonresult.getString("0")) + 1; i++) {
                        JSONArray jsonresultarray = jsonresult.getJSONArray(String.valueOf(i));
                        groupsActivity.myGroups.add(new Group(jsonresultarray.get(1).toString(), jsonresultarray.get(0).toString()));
                    }
                } else if (taskNumber == 1) {
                    groupsActivity.myGroups.add(new Group(groupsActivity.createGroupName.getText().toString(), result));
                }

                Log.i("STATUS", String.valueOf(conn.getResponseCode()));
                Log.i("MSG", conn.getResponseMessage());
                Log.i("RESPONSE", result);
                conn.disconnect();
                if (result.equals("notadmin")) {
                    return result;
                } else {
                    return "0";
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "1";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            GroupsActivity groupsActivity = activityReference.get();
            if (result.equals("notadmin")) {
                Toast.makeText(groupsActivity, groupsActivity.getResources().getText(R.string.notadmin), Toast.LENGTH_SHORT).show();
            }
            if (result.equals("0")) {
                groupsActivity.mySwipeRefreshLayout.setRefreshing(false);
                groupsActivity.groupsListAdapter.notifyDataSetChanged();
            }
            else if (result.equals("1")) {
                groupsActivity.mySwipeRefreshLayout.setRefreshing(false);
                Toast.makeText(groupsActivity, "Connection lost", Toast.LENGTH_LONG).show();
            }
        }
    }

    private static JSONObject createJSON(String task, String userid, String newGroupName, String groupId, String memberEmail) throws Exception {
        JSONObject jsonParam = new JSONObject();
        jsonParam.put("head", "groupsettings");
        jsonParam.put("userid", userid);
        jsonParam.put("task", task);
        jsonParam.put("groupid", groupId);
        if (task.equals("changename")) {
            jsonParam.put("newname", newGroupName);
        }
        if (task.equals("kickmember") || task.equals("addmember") || task.equals("makeadmin")) {
            jsonParam.put("email", memberEmail);
        }
        return jsonParam;
    }

}
