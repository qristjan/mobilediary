package ut.student.puusepp.kristjan.mobilediary;

import java.util.List;
import java.util.Set;


public class Group {

    String groupName;
    List<String> membersEmails;
    String serverId;

    public Group() {

    }

    public Group(String groupName, List<String> membersEmails, String serverId) {
        this.groupName = groupName;
        this.membersEmails = membersEmails;
        this.serverId = serverId;
    }

    public Group(String groupName, String serverId) {
        this.groupName = groupName;
        this.serverId = serverId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<String> getMembersEmails() {
        return membersEmails;
    }

    public void setMembersEmails(List<String> membersEmails) {
        this.membersEmails = membersEmails;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }
}
