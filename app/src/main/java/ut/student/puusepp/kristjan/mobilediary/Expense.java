package ut.student.puusepp.kristjan.mobilediary;

public class Expense {

    private int _id;
    private String _typeOfExpense;
    private String _descriptionOfExpense;
    private float _costOfExpense;
    private String _dateOfCreation;

    public Expense() {

    }

    public Expense(int _id, String _typeOfExpense, String _descriptionOfExpense, float _costOfExpense, String _dateOfCreation) {
        this._id = _id;
        this._typeOfExpense = _typeOfExpense;
        this._descriptionOfExpense = _descriptionOfExpense;
        this._costOfExpense = _costOfExpense;
        this._dateOfCreation = _dateOfCreation;
    }

    public int get_id() {

        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_typeOfExpense() {
        return _typeOfExpense;
    }

    public void set_typeOfExpense(String _typeOfExpense) {
        this._typeOfExpense = _typeOfExpense;
    }

    public String get_descriptionOfExpense() {
        return _descriptionOfExpense;
    }

    public void set_descriptionOfExpense(String _descriptionOfExpense) {
        this._descriptionOfExpense = _descriptionOfExpense;
    }

    public float get_costOfExpense() {
        return _costOfExpense;
    }

    public void set_costOfExpense(float _costOfExpense) {
        this._costOfExpense = _costOfExpense;
    }

    public String get_dateOfCreation() {
        return _dateOfCreation;
    }

    public void set_dateOfCreation(String _dateOfCreation) {
        this._dateOfCreation = _dateOfCreation;
    }

    @Override
    public String toString() {
        return _typeOfExpense + " " + _costOfExpense;
    }

}
