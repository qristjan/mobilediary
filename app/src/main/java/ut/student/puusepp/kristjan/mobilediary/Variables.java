package ut.student.puusepp.kristjan.mobilediary;

import java.util.Dictionary;

public class Variables {

    //static String SERVER_URL = "http://10.0.2.2:5000";
    static String SERVER_URL = "https://mobileapplication.ga";
    static String LOGIN = SERVER_URL + "/api/login";
    static String ADD_GROUP_REMINDERS = SERVER_URL + "/api/addgroupreminders";
    static String ADD_PERSONAL_REMINDERS = SERVER_URL + "/api/addpersonalreminders";
    static String SYNC_PERSONAL_REMINDERS = SERVER_URL + "/api/syncpersonal";
    static String ADD_NEW_GROUP = SERVER_URL + "/api/creategroup";
    static String GET_GROUP_REMINDERS = SERVER_URL + "/api/getgroupreminders";
    static String UPDATE_GROUP_REMINDER = SERVER_URL + "/api/updategroupreminder";
    static String GROUP_SETTINGS = SERVER_URL + "/api/groupsettings";
    static String REGISTER = SERVER_URL + "/api/createuser";
    static String GET_GROUPS = SERVER_URL + "/api/getmygroups";
    static String GET_GROUP_MEMBERS = SERVER_URL + "/api/getgroupmembers";
    static String DELETE_GROUP_REMINDER = SERVER_URL + "/api/deletegroupreminder";
    static String EXPENSES_DATA = SERVER_URL + "/api/expensesdata";

    static String TAG = "MOBILEAPP_LOG_TAG";
}
