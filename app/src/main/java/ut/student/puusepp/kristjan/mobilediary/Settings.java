package ut.student.puusepp.kristjan.mobilediary;

public class Settings {

    private int id;
    private int hidecompleted;
    private String locale;

    public Settings(int id, int hidecompeted, String locale) {
        this.id = id;
        this.hidecompleted = hidecompeted;
        this.locale = locale;
    }

    public Settings(int hidecompeted) {
        this.hidecompleted = hidecompeted;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHidecompleted() {
        return hidecompleted;
    }

    public void setHidecompleted(int hidecompleted) {
        this.hidecompleted = hidecompleted;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }


}
