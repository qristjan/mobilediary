package ut.student.puusepp.kristjan.mobilediary;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


public class CreateReminderActivity extends AppCompatActivity implements
        View.OnClickListener {

    private ImageButton btnDatePicker;
    private ImageButton btnTimePicker;
    private TextView reminderEndDate;
    private TextView reminderEndTime;

    String typeOfReminder;
    String groupId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createreminder);

        btnDatePicker = findViewById(R.id.btn_date);
        btnDatePicker.setOnClickListener(this);

        btnTimePicker = findViewById(R.id.btn_time);
        btnTimePicker.setOnClickListener(this);

        reminderEndDate = findViewById(R.id.reminderenddate);
        reminderEndTime = findViewById(R.id.reminderendtime);

        final DatabaseHandler db = new DatabaseHandler(this);
        final TextView title = findViewById(R.id.titleText);
        final TextView content = findViewById(R.id.contentText);

        final Intent intent = getIntent();

        /*
        "pcreate" = creating new personal reminder
        "gcreate" = creating new group reminder
        "ptrue" = changing existing personal reminder
        "gtrue" = changing existing group reminder
         */
        typeOfReminder = intent.getStringExtra("ce");

        if (typeOfReminder.equals("ptrue")) {
            int id = intent.getExtras().getInt("id");
            Reminder rem = db.getReminder(id);
            title.setText(rem.get_titleOfReminder());
            content.setText(rem.get_contentOfReminder());
            reminderEndDate.setText(rem.get_dateOfExpiration());
            reminderEndTime.setText(rem.get_timeOfExpiration());
        }

        if (typeOfReminder.equals("gtrue")) {
            groupId = intent.getStringExtra("groupid");
            title.setText(intent.getStringExtra("title"));
            content.setText(intent.getStringExtra("content"));
            reminderEndDate.setText(intent.getStringExtra("enddate"));
            reminderEndTime.setText(intent.getStringExtra("endtime"));
        }

        ImageButton deleteReminder = findViewById(R.id.deletereminder);
        if (typeOfReminder.equals("pcreate") || typeOfReminder.equals("gcreate")) deleteReminder.setVisibility(View.GONE);
        deleteReminder.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                  if (typeOfReminder.equals("ptrue")) {
                      Reminder reminder = db.getReminder(intent.getExtras().getInt("id"));
                      if (!reminder.get_sid().equals("local")) {
                          db.addToRemStat(reminder.get_sid(), 0);
                          db.deleteReminderById(String.valueOf(intent.getExtras().getInt("id")));
                      } else {
                          db.deleteReminderById(String.valueOf(intent.getExtras().getInt("id")));
                      }
                      startActivity(new Intent(CreateReminderActivity.this, MainActivity.class));
                      finish();
                  }
                  if (typeOfReminder.equals("gtrue")) {
                      new CreateRemindersAsyncTask(CreateReminderActivity.this, 5, Variables.DELETE_GROUP_REMINDER, db.getActiveUser().getUserid(), Integer.parseInt(intent.getStringExtra("groupid")), Integer.parseInt(intent.getStringExtra("reminderid"))).execute();
                  }
              }
          }
        );

        Button cancelReminder = findViewById(R.id.cancelreminder);
        cancelReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (typeOfReminder.equals("gtrue")) {
                    startActivity(new Intent(CreateReminderActivity.this, GroupRemindersActivity.class));
                    finish();
                }
                else {
                    startActivity(new Intent(CreateReminderActivity.this, MainActivity.class));
                    finish();
                }
            }
        });

        Button saveReminder = findViewById(R.id.savereminder);
        saveReminder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String newText = title.getText().toString();
                String newContent = content.getText().toString();
                String newDate = formatDate(reminderEndDate.getText().toString());
                String newTime = formatTime(reminderEndTime.getText().toString());

                if (typeOfReminder.equals("ptrue")) {
                    Reminder currentReminder = db.getReminder(intent.getExtras().getInt("id"));

                    currentReminder.set_titleOfReminder(newText);
                    currentReminder.set_contentOfReminder(newContent);
                    currentReminder.set_dateOfExpiration(newDate);
                    currentReminder.set_timeOfExpiration(newTime);
                    db.updateReminder(currentReminder);
                    startActivity(new Intent(CreateReminderActivity.this, MainActivity.class));
                    finish();
                }
                else if (typeOfReminder.equals("pcreate")) {
                    db.addReminder(new Reminder(newText, newContent, "", newDate, newTime, db.getActiveUser().getUserid(), "local"));
                    startActivity(new Intent(CreateReminderActivity.this, MainActivity.class));
                    finish();
                }
                else if (typeOfReminder.equals("gtrue")) {
                    new CreateRemindersAsyncTask(CreateReminderActivity.this,4, Variables.UPDATE_GROUP_REMINDER, db.getActiveUser().getUserid(),new GroupReminder("", intent.getStringExtra("reminderid"), newText, newContent, intent.getStringExtra("createddate"), newDate, newTime), Integer.parseInt(intent.getStringExtra("groupid"))).execute();
                }
                else if (typeOfReminder.equals("gcreate")) {
                    String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(Calendar.getInstance().getTime());
                    new CreateRemindersAsyncTask(CreateReminderActivity.this,1, Variables.ADD_GROUP_REMINDERS, db.getActiveUser().getUserid(), new GroupReminder(intent.getStringExtra("groupid"), "", newText, newContent, timeStamp, newDate, newTime), Integer.parseInt(intent.getStringExtra("groupid"))).execute();
                }

            }
        });

    }

    @Override
    public void onBackPressed() {
        if (typeOfReminder.equals("pcreate") || typeOfReminder.equals("ptrue")) {
            Log.i(Variables.TAG, "Going back to MainActivity from personal reminders");
            startActivity(new Intent(CreateReminderActivity.this, MainActivity.class));
            finish();

        }
        else if (typeOfReminder.equals("gcreate") || typeOfReminder.equals("gtrue")) {
            Intent intent = new Intent(CreateReminderActivity.this, GroupRemindersActivity.class);
            if (typeOfReminder.equals("gtrue")) intent.putExtra("groupid", groupId);
            startActivity(intent);
            finish();
        }
        else {
            moveTaskToBack(true);
        }
    }

    public String formatDate(String date) {
        Log.i(Variables.TAG, date);
        try {
            if (date.equals("")) {
                return "";
            } else {
                String[] pieces = date.split("\\.");
                for (int i = 0; i < pieces.length; i++) {
                    pieces[i] = add0IfNecessary(Integer.parseInt(pieces[i]));
                }
                return pieces[0] + "." + pieces[1] + "." + pieces[2];
            }
        } catch (Exception e) {
            return "";
        }

    }

    public String formatTime(String time) {
        try {
            if (time.equals("")) {
                return "";
            } else {
                String[] pieces = time.split(":");
                return add0IfNecessary(Integer.parseInt(pieces[0])) + ":" + add0IfNecessary(Integer.parseInt(pieces[1]));
            }
        } catch (Exception e) {
            return "";
        }
    }

    public String add0IfNecessary(int a) {
        String temp = String.valueOf(a);
        if (a < 10) {
            temp = "0" + a;
        }
        return temp;
    }


    @Override
    public void onClick(View v) {

        if (v == btnDatePicker) {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {
                            String endDate = add0IfNecessary(dayOfMonth) + "." + add0IfNecessary((monthOfYear + 1)) + "." + year;
                            reminderEndDate.setText(endDate);

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }
        if (v == btnTimePicker) {

            // Get Current Time
            final Calendar c = Calendar.getInstance();
            int mHour = c.get(Calendar.HOUR_OF_DAY);
            int mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    new TimePickerDialog.OnTimeSetListener() {

                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay,
                                              int minute) {
                            String endTime = add0IfNecessary(hourOfDay) + ":" + add0IfNecessary(minute);
                            reminderEndTime.setText(endTime);
                        }
                    }, mHour, mMinute, true);
            timePickerDialog.show();
        }
    }

    private static class CreateRemindersAsyncTask extends AsyncTask<Void, Void, Integer> {

        private WeakReference<CreateReminderActivity> activityReference;
        private Integer taskNumber;
        private String API_URL;
        private String userId;

        private GroupReminder groupReminder;

        private Integer groupId;
        private Integer groupReminderId;

        CreateRemindersAsyncTask(CreateReminderActivity context, Integer taskNumber, String API_URL, String userId, GroupReminder groupReminder, Integer groupId) {
            activityReference = new WeakReference<>(context);
            this.taskNumber = taskNumber;
            this.API_URL = API_URL;
            this.userId = userId;
            this.groupId = groupId;
            this.groupReminder = groupReminder;

        }

        CreateRemindersAsyncTask(CreateReminderActivity context, Integer taskNumber, String API_URL, String userId, Integer groupId, Integer groupReminderId) {
            activityReference = new WeakReference<>(context);
            this.taskNumber = taskNumber;
            this.API_URL = API_URL;
            this.userId = userId;
            this.groupId = groupId;
            this.groupReminderId = groupReminderId;

        }

        protected Integer doInBackground(Void... voids) {

            try {
                URL url = new URL(API_URL);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json");
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setConnectTimeout(5000);

                JSONObject jsonParam = new JSONObject();

                if (taskNumber == 1) jsonParam.put("head", "addgroupreminders");
                if (taskNumber == 4) jsonParam.put("head", "updategroupreminder");
                if (taskNumber == 5) jsonParam.put("head", "deletegroupreminder");

                jsonParam.put("userid", userId);

                if (taskNumber == 1 || taskNumber == 4) {
                    JSONArray jArray = new JSONArray();
                    JSONObject reminderInfo = new JSONObject();
                    if (taskNumber == 1 || taskNumber == 4) {
                        if (taskNumber == 1) {
                            reminderInfo.put("groupid", groupReminder.getGroupId());
                            reminderInfo.put("createddate", groupReminder.get_dateOfCreation());
                        }
                        if (taskNumber == 4) reminderInfo.put("reminderid", groupReminder.getReminderId());
                        reminderInfo.put("title", groupReminder.get_titleOfReminder());
                        reminderInfo.put("content", groupReminder.get_contentOfReminder());
                        reminderInfo.put("enddate", groupReminder.get_dateOfExpiration());
                        reminderInfo.put("endtime", groupReminder.get_timeOfExpiration());
                        jArray.put(reminderInfo);
                        jsonParam.put("groupreminder", jArray);
                    }
                }

                if (taskNumber == 5) {
                    jsonParam.put("groupid", groupId);
                    jsonParam.put("reminderid", groupReminderId);
                }



                Log.i("JSON", jsonParam.toString());

                DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                os.writeBytes(jsonParam.toString());

                os.flush();
                os.close();

                final String result = IOUtils.toString(conn.getInputStream(), StandardCharsets.UTF_8);
                if (taskNumber == 5 && result.equals("notadmin")) return 3;

                Log.i("STATUS", String.valueOf(conn.getResponseCode()));
                Log.i("MSG", conn.getResponseMessage());
                Log.i("RESPONSE", result);
                conn.disconnect();
                return 0;
            } catch (Exception e) {
                e.printStackTrace();
                return 1;
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            CreateReminderActivity groupsActivity = activityReference.get();
            if (result == 3) {
                Toast.makeText(groupsActivity, groupsActivity.getResources().getText(R.string.notadmin), Toast.LENGTH_SHORT).show();
            } else if (result == 0) {
                Intent intent = new Intent(groupsActivity, GroupRemindersActivity.class);
                intent.putExtra("groupid", String.valueOf(groupId));
                groupsActivity.startActivity(intent);
                groupsActivity.finish();
            } else if (result == 1) {
                Toast.makeText(groupsActivity, groupsActivity.getResources().getText(R.string.wrong), Toast.LENGTH_SHORT).show();
            }
        }
    }

}

