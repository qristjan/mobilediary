package ut.student.puusepp.kristjan.mobilediary;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;


public class CreateExpenseActivity extends AppCompatActivity {

    Button btnDatePicker;
    private int mYear, mMonth, mDay;
    String cc = "o";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createexpense);

        final Intent intent = getIntent();
        final String var = intent.getStringExtra("expense");

        final Context context = this;
        final DatabaseHandler db = new DatabaseHandler(this);
        final EditText cost = findViewById(R.id.cost);
        final EditText desc = findViewById(R.id.contentTextExpense);

        btnDatePicker = findViewById(R.id.btn_date2);
        btnDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String date = add0IfNecessary(dayOfMonth) + "." + add0IfNecessary((monthOfYear + 1)) + "." + year;
                                btnDatePicker.setText(date);
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        final Button chooseCategory = findViewById(R.id.choosecategorybtn);
        chooseCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.dialog_choosecategory);

                final Button cat_food = dialog.findViewById(R.id.foodExpense);
                final Button cat_ente = dialog.findViewById(R.id.entertainmentExpense);
                final Button cat_tran = dialog.findViewById(R.id.transportExpense);
                final Button cat_clot = dialog.findViewById(R.id.clothesExpense);
                final Button cat_othe = dialog.findViewById(R.id.otherExpense);

                cat_food.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chooseCategory.setText(getResources().getText(R.string.food));
                        cc = "f";
                        dialog.dismiss();
                    }
                });

                cat_ente.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chooseCategory.setText(getResources().getText(R.string.entertainment));
                        cc = "e";
                        dialog.dismiss();
                    }
                });

                cat_tran.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chooseCategory.setText(getResources().getText(R.string.transport));
                        cc = "t";
                        dialog.dismiss();
                    }
                });

                cat_clot.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chooseCategory.setText(getResources().getText(R.string.clothes));
                        cc = "c";
                        dialog.dismiss();
                    }
                });

                cat_othe.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chooseCategory.setText(getResources().getText(R.string.other));
                        cc = "o";
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });


        if (var.equals("existing")) {
            cost.setText(String.valueOf(intent.getFloatExtra("cost", 0)));
            desc.setText(intent.getStringExtra("description"));
            btnDatePicker.setText(intent.getStringExtra("date"));
            chooseCategory.setText(intent.getStringExtra("type"));
        }

        Button cancelExpense = findViewById(R.id.cancelexpense);
        cancelExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CreateExpenseActivity.this, ExpensesActivity.class));
                finish();
            }
        });

        Button saveExpense = findViewById(R.id.saveexpense);
        saveExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cost.getText().toString().isEmpty()) {
                    Toast.makeText(getApplicationContext(), getResources().getText(R.string.fillincost), Toast.LENGTH_SHORT).show();
                } else {

                    Expense expense = new Expense();
                    if (var.equals("existing"))
                        expense = db.getExpense(intent.getExtras().getInt("id"));

                    expense.set_costOfExpense(Float.parseFloat(cost.getText().toString()));
                    expense.set_descriptionOfExpense(desc.getText().toString());
                    expense.set_typeOfExpense(cc);
                    expense.set_dateOfCreation(btnDatePicker.getText().toString());

                    if (var.equals("existing")) db.updateExpense(expense);
                    else db.addExpense(expense);

                    Intent intent1 = new Intent(CreateExpenseActivity.this, ExpensesActivity.class);
                    startActivity(intent1);
                    finish();
                }

            }
        });

    }

    public String add0IfNecessary(int a) {
        String temp = String.valueOf(a);
        if (a < 10) {
            temp = "0" + a;
        }
        return temp;
    }
}
