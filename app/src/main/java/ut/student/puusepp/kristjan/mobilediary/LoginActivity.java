package ut.student.puusepp.kristjan.mobilediary;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class LoginActivity extends AppCompatActivity {

    EditText email;
    EditText password;
    final DatabaseHandler db = new DatabaseHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = findViewById(R.id.loginusername);
        password = findViewById(R.id.loginpassword);


        Button loginBtn = findViewById(R.id.loginbtn);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (email.getText().toString().equals("") || password.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), getResources().getText(R.string.fieldscannotbeempty), Toast.LENGTH_SHORT).show();
                } else {
                    //LoginAsyncTask loginAsyncTask = new LoginAsyncTask();
                    //loginAsyncTask.execute();
                    new LoginAsyncTask(email.getText().toString(), password.getText().toString(), Variables.LOGIN, LoginActivity.this).execute();
                }
            }
        });

        Button goToRegister = findViewById(R.id.goToRegister);
        goToRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                finish();
            }
        });
    }


    private static class LoginAsyncTask extends AsyncTask<Void, Void, JSONObject> {

        private String email;
        private String password;
        private String API_URL;
        private WeakReference<LoginActivity> activityReference;

        LoginAsyncTask(String email, String password, String API_URL, LoginActivity context) {
            this.email = email;
            this.password = password;
            this.API_URL = API_URL;
            activityReference = new WeakReference<>(context);
        }

        protected JSONObject doInBackground(Void... voids) {
            try {
                URL url = new URL(API_URL);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json");
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setConnectTimeout(5000);

                JSONObject jsonParam = new JSONObject();
                jsonParam.put("head", "login");
                jsonParam.put("email", email);
                jsonParam.put("password", password);

                DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                //os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                os.writeBytes(jsonParam.toString());

                os.flush();
                os.close();

                final String result = IOUtils.toString(conn.getInputStream(), StandardCharsets.UTF_8);

                Log.i("STATUS", String.valueOf(conn.getResponseCode()));
                Log.i("MSG", conn.getResponseMessage());
                Log.i("RESPONSE", result);
                conn.disconnect();

                return new JSONObject(result);

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        }

        @Override
        protected void onPostExecute(JSONObject result) {
            LoginActivity loginActivity = activityReference.get();
            if (result != null ) {
                try {
                    switch (result.getString("status")) {
                        case "loggedin":
                            if (loginActivity.db.getAllUsersEmails().contains(email)) {
                                loginActivity.db.updateUserActivityToFalse();
                                loginActivity.db.updateUserActivityToTrue(email);
                            } else {
                                loginActivity.db.addUser(new User(result.getString("userid"), email, "t"));
                            }
                            Intent intent = new Intent(loginActivity, MainActivity.class);
                            loginActivity.startActivity(intent);
                            loginActivity.finish();
                            break;
                        case "wrongcreds":
                            Log.i(Variables.TAG, "Wrong credentials when logging");
                            Toast.makeText(loginActivity, loginActivity.getResources().getText(R.string.wrongcredentials), Toast.LENGTH_SHORT).show();
                            break;
                        case "notlogged":
                            Toast.makeText(loginActivity, loginActivity.getResources().getText(R.string.wrong), Toast.LENGTH_SHORT).show();
                            break;
                    }
                } catch (JSONException e) {
                    Toast.makeText(loginActivity, loginActivity.getResources().getText(R.string.wrong), Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(loginActivity, loginActivity.getResources().getText(R.string.wrong), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
