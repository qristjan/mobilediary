package ut.student.puusepp.kristjan.mobilediary;

public class User {

    private int id;
    private String userid;
    private String email;
    private String active;

    public User(int id, String userid, String email, String active) {
        this.id = id;
        this.userid = userid;
        this.email = email;
        this.active = active;
    }

    public User(String userid, String email, String active) {
        this.userid = userid;
        this.email = email;
        this.active = active;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
